package OSMData;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.kuleuven.masterthesis.server.MessageServiceImpl;

/**
 * 
 * A class that runs PostgreSQL queries in separate threads
 * 
 * @author Michiel Pauwels
 *
 */

public class QueryThread extends Thread{

	protected ResultSet rs;
	private Connection conn;
	private String query;
	
	/**
	 * Constructor that sets the query to be executed
	 * @param q The query string
	 */
	public QueryThread(String q){
		query = q;
	}

	@Override
	/**
	 * Method that executes a query
	 */
	public void run() {
		System.out.println(query);
		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+MessageServiceImpl.dBName,MessageServiceImpl.Username, MessageServiceImpl.Password);
			Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(query);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method that closes connection
	 */
	public void closeconn(){
		try {
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

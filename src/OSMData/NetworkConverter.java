package OSMData;

import java.util.ArrayList;

import Graph.*;
import OSM.*;

/**
 * 
 * A class that converts an OSM_like network to a graph network
 * 
 * @author Michiel Pauwels
 *
 */

public class NetworkConverter {

	private Network network = new Network();
	
	/**
	 * Constructor class that converts the network immediately.
	 * First the OSM network gets converted to a graph network, then
	 * overhead is removed and descriptors are calculated.
	 * 
	 * @param netw The OSM network
	 */
	public NetworkConverter(OSM_Network netw){
		long startTime = System.nanoTime();
		this.ConvertNetwork(netw);
		this.RemoveDoubles();
		this.getNeighbourEdges();
		this.calculateDescriptors();
		long endTime = System.nanoTime();
		System.out.println("Omzetten netwerk + overhead verwijderen: "
				+ Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
	}
	
	/**
	 * Method that converts an OSM network into a graph network
	 * @param netw the OSM network
	 */
	private void ConvertNetwork(OSM_Network netw) {
		network.setMaxlat(netw.getMaxlat());
		network.setMinlat(netw.getMinlat());
		network.setMaxlon(netw.getMaxlon());
		network.setMinlon(netw.getMinlon());
		int idcounter = 1;
		DescriptorOSM desc = null;
		for (int i = 0; i < netw.getWays().size(); i++) {
			OSM_Way huidigeWeg = netw.getWays().get(i);
			Edge NewArc = null;
			for (int u = 0; u < huidigeWeg.getPath().size(); u++) {
				OSM_Node huidigeNode = huidigeWeg.getPath().get(u);
				long Nodeid = huidigeNode.getId();
				double Nodelat = huidigeNode.getLat();
				double Nodelon = huidigeNode.getLon();
				
				//First node --> new path
				if (u == 0) {
					Node NewNode = network.CheckNode(Nodeid);
					if (NewNode == null) {
						NewNode = new Node(Nodeid, Nodelat, Nodelon);
						network.AddInters(NewNode);
					}
					NewArc = new Edge(idcounter, NewNode);
					NewArc.setOSMid(huidigeWeg.getId());
					desc = new DescriptorOSM(NewArc.getId());
					desc.setBicyc(huidigeWeg.getBicyc());
					desc.setCyc(huidigeWeg.getCyc());
					desc.setType(huidigeWeg.getType());
					desc.setNum_of_lanes(huidigeWeg.getNum_of_lanes());
					desc.setOneWay(huidigeWeg.isOneWay());
					desc.setTraffic_lights(huidigeNode.isTraffic_signals());
				}
				//last node --> end of path
				if (u == huidigeWeg.getPath().size() - 1) {
					Node NewNode = network.CheckNode(Nodeid);
					if (NewNode == null) {
						NewNode = new Node(Nodeid, Nodelat, Nodelon);
						network.AddInters(NewNode);
					}
					NewArc.setB(NewNode);
					if (!desc.isTraffic_lights())
						desc.setTraffic_lights(huidigeNode.isTraffic_signals());
					NewArc.setDescriptor(desc);
					network.AddArc(NewArc);
					idcounter++;
				} 
				//common node --> splice into two paths
				else {
					if (huidigeNode.getPaths().size() >= 2) { 
						Node NewNode = network.CheckNode(Nodeid);
						if (NewNode == null) {
							NewNode = new Node(Nodeid, Nodelat, Nodelon);
							network.AddInters(NewNode);
						}
						NewArc.setB(NewNode);
						if (!desc.isTraffic_lights())
							desc.setTraffic_lights(huidigeNode.isTraffic_signals());
						NewArc.setDescriptor(desc);
						network.AddArc(NewArc);
						idcounter++;
						NewArc = new Edge(idcounter, NewNode);
						NewArc.setOSMid(huidigeWeg.getId());
						desc = new DescriptorOSM(NewArc.getId());
						desc.setBicyc(huidigeWeg.getBicyc());
						desc.setCyc(huidigeWeg.getCyc());
						desc.setType(huidigeWeg.getType());
						desc.setNum_of_lanes(huidigeWeg.getNum_of_lanes());
						desc.setOneWay(huidigeWeg.isOneWay());
						desc.setTraffic_lights(huidigeNode.isTraffic_signals());
					} 
					//no intersection --> simpel node 
					else { 
						NewArc.AddtoPath(new ShapePoint(Nodeid, Nodelat, Nodelon));
					}
				}
			}
		}
		System.out.println("IDcounter: "+idcounter);
		System.out.println("Netwerk met " + network.getNodes().size() + " Nodes & " + network.getArcs().size() + " Arcs");
	}
	
	/**
	 * Method that checks for nodes which mutual distance is zero
	 */
	private void RemoveDoubles() {
		int falsecounter = 0;
		for (int i = 0; i < network.getArcs().size(); i++) {
			Node A = network.getArcs().get(i).getA();
			Node B = network.getArcs().get(i).getB();
			if (A.equals(B) || A.CalculateDist(B) == 0) {
				network.getArcs().remove(i);
				i--;
				falsecounter++;
			}
			else {
				for (int z = 0; z < network.getArcs().get(i).getPath().size(); z++) {
					if ((A.CalculateDist(network.getArcs().get(i).getPath().get(z)) == 0)
							|| (B.CalculateDist(network.getArcs().get(i).getPath().get(z)) == 0)) {
						network.getArcs().get(i).getPath().remove(z);
						z--;
						falsecounter++;
					}
				}
				for (int u = 0; u < network.getArcs().get(i).getPath().size(); u++) {
					for (int z = u + 1; z < network.getArcs().get(i).getPath().size(); z++) {
						if (network.getArcs().get(i).getPath().get(u)
								.CalculateDist(network.getArcs().get(i).getPath().get(z)) == 0) {
							network.getArcs().get(i).getPath().remove(z);
							z--;
							falsecounter++;
						}
					}
				}
			}
		}
		System.out.println("Aantal nodes op dezelfde plek verwijderd: " + falsecounter);
	}


	private void calculateDescriptors() {
		for(int i=0; i<network.getArcs().size(); i++){
			Edge e = network.getArcs().get(i);
			if(e.getDescriptor()==null){e.setDescriptor(new DescriptorOSM(e));}
			else {e.getDescriptor().calculateDesc(e);}
		}
	}

	private void getNeighbourEdges() {
		for (int i = 0; i < network.getNodes().size(); i++) {
			network.getNodes().get(i).setEdges(new ArrayList<Edge>());
		}
		for (int i = 0; i < network.getArcs().size(); i++) {
			Edge e = network.getArcs().get(i);
			e.getA().getEdges().add(e);
			e.getB().getEdges().add(e);
		}
	}
	
	public Network getNetwork() {
		return network;
	}
	
}
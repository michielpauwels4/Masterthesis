package OSMData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import Graph.Network;
import OSM.OSM_Network;
import OSM.OSM_Node;
import OSM.OSM_Way;
import UserData.GPSPoint;
import UserData.Trip;

/**
 * 
 * A class that can fetch an OSM network in different ways:
 * 	- Through an API (Overpass)
 * 	- Through a Database (PostgreSQL)
 * 	- Through an OSM XML file
 * 
 * @author Michiel Pauwels
 *
 */

public class NetworkFetcher {
	
	private ArrayList<GPSPoint> cluster;
	public ArrayList<Trip> trips;
	
	private float minLat;
	private float maxLat;
	private float minLon;
	private float maxLon;
	
	/**
	 * Constructor of the class with clusterdata and trips.
	 * The bounds are derived from the clusterdata and the trips inside those bounds are kept.
	 * @param Cluster The set of gpspoints
	 * @param trips The set of trips
	 */
	public NetworkFetcher(ArrayList<GPSPoint> Cluster, ArrayList<Trip> trips){
		cluster = Cluster;
		calculateBounds();
		selectTrips(trips);
	}
	
	/**
	 * Constructor of the class with bounds.
	 * @param minLatit minimum latitude
	 * @param maxLatit maximum latitude 
	 * @param minLongit minimum longitude
	 * @param maxLongit maximum longitude
	 */
	public NetworkFetcher(float minLatit, float maxLatit, float minLongit, float maxLongit){
		minLat = minLatit;
		maxLat = maxLatit;
		minLon = minLongit;
		maxLon = maxLongit;
	}
	
	private void calculateBounds() {
		float minLat=Float.MAX_VALUE,maxLat=Float.NEGATIVE_INFINITY,minLon=Float.MAX_VALUE,maxLon=Float.NEGATIVE_INFINITY;
		for(int u=0; u<cluster.size(); u++){
			GPSPoint p = cluster.get(u);
			if(p.getLat()>maxLat){maxLat = (float) p.getLat();}
			if(p.getLat()<minLat){minLat = (float) p.getLat();}
			if(p.getLon()>maxLon){maxLon = (float) p.getLon();}
			if(p.getLon()<minLon){minLon = (float) p.getLon();}
		}			
		this.maxLat = maxLat;
		this.minLat = minLat;
		this.maxLon = maxLon;
		this.minLon = minLon;
	}
	
	private void selectTrips(ArrayList<Trip> t){
		ArrayList<Trip> selected = new ArrayList<Trip>();
		for(int i=0; i<t.size(); i++){
			Trip trip = t.get(i);
			for(int u=0; u<trip.getRoute().size(); u++){
				GPSPoint p = trip.getRoute().get(u);
				if(p.getLat()<maxLat && p.getLat()>minLat && p.getLon()<maxLon && p.getLon()>minLon){
					selected.add(trip);
					break;
				}
			}
		}
		System.out.println(selected.size()+" trips in netwerk!");
		trips = selected;
	}

	
	/**
	 * Method that fetches a network via the Overpass API with a bounding box derived from the bounds.
	 * The osm network gets directly converted to a graph network
	 * @return A network object
	 * @throws InterruptedException caused if a sleep command gets interrupted
	 */
	public Network FetchNetwork() throws InterruptedException{
		
		long startTime = System.nanoTime();	
		OSM_Network onet = null;
		int sleeptime = 90;
		
		while(sleeptime!=0){
			try {
				URL init = new URL("http://overpass-api.de/api/kill_my_queries");
				HttpURLConnection conn = (HttpURLConnection) init.openConnection();
				conn.connect();
				URL website = new URL("https://overpass-api.de/api/map?bbox="+minLon+","+minLat+","+maxLon+","+maxLat);
				System.out.println("URL: "+website.toString());
				URL osm = new URL(website.toString());
				HttpURLConnection connection = (HttpURLConnection) osm.openConnection();
				OSM_SAXParser parser = new OSM_SAXParser(connection.getInputStream());
				onet = parser.leesIn();
				sleeptime = 0;
			} catch (IOException e) {
				System.out.println("Failed! Sleep for "+sleeptime+" seconds");
				TimeUnit.SECONDS.sleep(sleeptime);
				sleeptime = sleeptime*2;
			}
		}
		
		addPathsToNodes(onet);
		analyseNodes(onet);
		NetworkConverter c = new NetworkConverter(onet);
		long endTime = System.nanoTime();
		System.out.println("Opvragen netwerk: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
		return c.getNetwork();
	}
	
	/**
	 * Method that fetches a network from an OSM XML file
	 * @param filename
	 * @return A network object
	 */
	public Network FetchNetworkFile(String filename){
		
		long startTime = System.nanoTime();
		NetworkConverter c = null;		
		
		OSM_SAXParser parser;
		try {
			parser = new OSM_SAXParser(new FileInputStream(new File(filename)));
			c = new NetworkConverter(parser.leesIn());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		long endTime = System.nanoTime();
		System.out.println("Opvragen netwerk: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
		return c.getNetwork();
	}
	
	/**
	 * Method that fetches OSM data from a database. 
	 * The queries are executed in separated threads to speed up the process.
	 * @return A network object
	 * @throws SQLException Thrown by database errors
	 */
	public Network FetchNetworkDB() throws SQLException {
		
		long startTime = System.nanoTime();
		
		HashMap<Long, List<Long>> waynodes = new HashMap<Long, List<Long>>();
		OSM_Network onet = new OSM_Network();
		onet.setMaxlat(maxLat);
		onet.setMaxlon(maxLon);
		onet.setMinlat(minLat);
		onet.setMinlon(minLon);
		
		String query = "SELECT * FROM way WHERE wid in (SELECT DISTINCT wid FROM way_nodes WHERE nid in (SELECT nid FROM node WHERE ((lat > "+minLat+") AND (lat < "+maxLat+") AND (lon > "+minLon+") AND (lon < "+maxLon+")) ));" ;
		QueryThread waysq = new QueryThread(query);
		query = "SELECT * FROM way_nodes WHERE nid in (SELECT nid FROM node WHERE ((lat > "+minLat+") AND (lat < "+maxLat+") AND (lon > "+minLon+") AND (lon < "+maxLon+")) ) ORDER BY wid;";
		QueryThread way_nodesq = new QueryThread(query);
		query = "SELECT * FROM Node WHERE ( (lat > "+minLat+") AND (lat < "+maxLat+") AND (lon > "+minLon+") AND (lon < "+maxLon+") );" ;
		query = "SELECT * FROM Node WHERE nid in (SELECT DISTINCT nid FROM way_nodes WHERE wid in (SELECT DISTINCT wid FROM way_nodes WHERE nid in (SELECT nid FROM node WHERE ((lat > "+minLat+") AND (lat < "+maxLat+") AND (lon > "+minLon+") AND (lon < "+maxLon+")) )));";
		QueryThread nodesq = new QueryThread(query);
		
		nodesq.start();
		waysq.start();
		way_nodesq.start();
		
		try {
			nodesq.join();
			ResultSet rs = nodesq.rs;
	        while ( rs.next() ) {
	        	boolean bool = false;
	        	if(rs.getString("traffic_signals").equals("t")){bool = true;}
	        	onet.addNode(new OSM_Node((int) rs.getLong("nid"),Double.parseDouble(rs.getString("lat")),Double.parseDouble(rs.getString("lon")),bool));
			}
	        nodesq.closeconn();
	        System.out.println("aantal resultaten: "+onet.getNodes().size());
	        
	        waysq.join();
	        rs = waysq.rs;
	        while ( rs.next() ) {
	        	boolean bool = false;
	        	if(rs.getString("oneway").equals("t")){bool=true;}
	        	onet.addWay((new OSM_Way(rs.getLong("wid"),rs.getString("highway"),rs.getString("cycleway"),rs.getString("bicycle"),bool, Integer.parseInt(rs.getString("lanes")))));
	        }
	        waysq.closeconn();
	        
	        way_nodesq.join();
	        rs = way_nodesq.rs;
	        while ( rs.next() ){
	        	if(!waynodes.containsKey(rs.getLong("wid"))){waynodes.put(rs.getLong("wid"), new ArrayList<Long>());}
	        	waynodes.get(rs.getLong("wid")).add(rs.getLong("nid"));
	        }
	        way_nodesq.closeconn();
	        for(int i=0; i<onet.getWays().size(); i++){
	        	List<Long> nodes = waynodes.get(onet.getWays().get(i).getId());
	        	for(int u=0; u<nodes.size(); u++){
	        		if(onet.SearchNode(nodes.get(u))!=null) onet.getWays().get(i).AddNode(onet.SearchNode(nodes.get(u)));
	        	}
	        }
	        System.out.println("aantal resultaten: "+onet.getWays().size());       
	        rs.close();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		addPathsToNodes(onet);
		analyseNodes(onet);
		NetworkConverter c = new NetworkConverter(onet);
		long endTime = System.nanoTime();
		System.out.println("Opvragen netwerk: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
		return c.getNetwork();
	}
	
	/**
	 * The method that fetches networkdata from a given set of way id's
	 * @param wids A set of way id's
	 * @return A network object
	 */
	public Network getNetworkFromIDsDB(ArrayList<Long> wids){
		
		HashMap<Long, List<Long>> waynodes = new HashMap<Long, List<Long>>();
		OSM_Network onet = new OSM_Network();
		onet.setMaxlat(maxLat);
		onet.setMaxlon(maxLon);
		onet.setMinlat(minLat);
		onet.setMinlon(minLon);
		
		long startTime = System.nanoTime();
		
		int count = 0;
		int maxwids = wids.size();
		while(count < wids.size()){
			String queryextra = "";
			int max = count;
			for(int i=max; i<max+maxwids;i++){
				if(i>=wids.size()){break;}
				count++;
				if(i==max+maxwids-1){queryextra += "wid = "+wids.get(i);}
				else {queryextra += "wid = "+wids.get(i)+" OR ";}
			}
			String query = "SELECT * FROM way WHERE ("+queryextra+");";
			QueryThread waysq = new QueryThread(query);
			query = "SELECT * FROM way_nodes WHERE wid in (SELECT wid FROM way WHERE ("+queryextra+"));";
			QueryThread way_nodesq = new QueryThread(query);
			query = "SELECT * FROM Node WHERE nid in (SELECT nid FROM way_nodes WHERE wid in (SELECT wid FROM way WHERE ("+queryextra+")));";
			QueryThread nodesq = new QueryThread(query);
			
			waysq.run();
			nodesq.run();
			way_nodesq.run();
			
			try {
				waysq.join();
				ResultSet rs = waysq.rs;
				while ( rs.next() ) {
					boolean bool = false;
		        	if(rs.getString("oneway").equals("t")){bool=true;}
		        	onet.addWay((new OSM_Way(rs.getLong("wid"),rs.getString("highway"),rs.getString("cycleway"),rs.getString("bicycle"),bool, Integer.parseInt(rs.getString("lanes")))));
		        }
				waysq.closeconn();
				
				nodesq.join();
				rs = nodesq.rs;
				while ( rs.next() ) {
					boolean bool = false;
		        	if(rs.getString("traffic_signals").equals("t")){bool = true;}
		        	onet.addNode(new OSM_Node((int) rs.getLong("nid"),Double.parseDouble(rs.getString("lat")),Double.parseDouble(rs.getString("lon")),bool));
				}
				nodesq.closeconn();
				
				way_nodesq.join();
				rs = way_nodesq.rs;
		        while ( rs.next() ){
		        	if(!waynodes.containsKey(rs.getLong("wid"))){waynodes.put(rs.getLong("wid"), new ArrayList<Long>());}
		        	waynodes.get(rs.getLong("wid")).add(rs.getLong("nid"));
		        }
		        for(int i=0; i<onet.getWays().size(); i++){
		        	List<Long> nodes = waynodes.get(onet.getWays().get(i).getId());
		        	for(int u=0; u<nodes.size(); u++){
		        		if(onet.SearchNode(nodes.get(u))!=null) onet.getWays().get(i).AddNode(onet.SearchNode(nodes.get(u)));
		        	}
		        }
				way_nodesq.closeconn();
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		addPathsToNodes(onet);
		analyseNodes(onet);
		NetworkConverter c = new NetworkConverter(onet);
		long endTime = System.nanoTime();
		System.out.println("Opvragen netwerk: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
		return c.getNetwork();
	}
	
	private void addPathsToNodes(OSM_Network network) {
		for (int i = 0; i < network.getWays().size(); i++) {
			for (int u = 0; u < network.getWays().get(i).getPath().size(); u++) {
				network.getWays().get(i).getPath().get(u).addWay(network.getWays().get(i));
			}
		}
	}

	private void analyseNodes(OSM_Network network) {
		long startTime = System.nanoTime();
		int counter = 0;
		int falsecounter = 0;
		int simplecounter = 0;
		for (int i = 0; i < network.getNodes().size(); i++) {
			if (network.getNodes().get(i).getPaths().size() == 0) { //geen enkel pad: overbodig
				network.getNodes().remove(i);
				i--;
				falsecounter++;
			} else if (network.getNodes().get(i).getPaths().size() == 1) {
				simplecounter++;
			} else { //meerdere paden: intersection
				counter++;
			}
		}
		long endTime = System.nanoTime();
		System.out.println("Berekenen Gemeenschappelijke Nodes: "
				+ Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
		System.out.println(simplecounter + " Simpele Nodes, " + counter + " Gemeenschappelijke Nodes, " + falsecounter
				+ " Nutteloze Nodes verwijderd");
	}
	
}
package OSMData;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.kuleuven.masterthesis.server.MessageServiceImpl;

import OSM.Bicycle;
import OSM.Cycleway;
import OSM.Highway;
import OSM.OSM_Node;
import OSM.OSM_Way;

/**
 * 
 * A class that parses an OSM XML file and inserts it directly into a database (PostgreSQL)
 * 
 * @author Michiel Pauwels
 *
 */

public class OSMtoDB extends DefaultHandler {

	private SAXParser parser;
	private InputStream stroom;
	private Connection conn;

	private OSM_Node huidigeNode;
	private OSM_Way huidigeWay;
	private ArrayList<Long> huidigeWayNodes = new ArrayList<Long>();

	private boolean nieuw=true;
	private boolean way;
	private boolean validWay;
	private boolean area;
	private boolean node;

	/**
	 * Constructor of the object.
	 * Opens an InputStream from a filename and deletes every OSM related table in the database
	 * @param fileName The name of the OSM XML file
	 */
	public OSMtoDB(String fileName) {
		
		try {
			stroom = new FileInputStream(fileName);
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+MessageServiceImpl.dBName,MessageServiceImpl.Username, MessageServiceImpl.Password);
			conn.setAutoCommit(true);
			System.out.println("Opened database successfully");
			
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("DELETE FROM Way_Nodes;");
			stmt.executeUpdate("DELETE FROM Node;");
			stmt.executeUpdate("DELETE FROM Way;");
			System.out.println("Deleted tables");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}  
		
		try {
			parser = SAXParserFactory.newInstance().newSAXParser();
		} catch (ParserConfigurationException e) {
			System.out.println(e);
		} catch (SAXException e) {
			System.out.println(e);
		}
	}

	/**
	 * Method that parses the InputStream. 
	 * Also removes overhead from the databank.
	 */
	public void leesIn() {
		long startTime = System.nanoTime();
		System.out.println("Start XML Parsing");
		try {
			parser.parse(stroom, this);
			removeOverhead();
			stroom.close();
		} catch (SAXException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		long endTime = System.nanoTime();
		System.out.println("Inlezen XML: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
	}
	
	private void removeOverhead(){
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("CREATE TABLE node2 AS SELECT * FROM node WHERE 1=2;");
			stmt.executeUpdate("ALTER TABLE node2 ADD PRIMARY KEY (nid);");
			stmt.executeUpdate("INSERT INTO node2 (nid) (SELECT DISTINCT nid from way_nodes);");
			stmt.executeUpdate("UPDATE node2 SET lat = node.lat, lon = node.lon, traffic_signals = node.traffic_signals FROM node WHERE node2.nid = node.nid;");
			stmt.executeUpdate("ALTER TABLE node RENAME TO node3;");
			stmt.executeUpdate("ALTER TABLE node2 RENAME TO node;");
			stmt.executeUpdate("ALTER TABLE node3 RENAME TO node2;");
			stmt.executeUpdate("ALTER TABLE way_nodes DROP CONSTRAINT way_nodes_nid_fkey;");
			stmt.executeUpdate("DROP TABLE node2;");
			stmt.executeUpdate("ALTER TABLE way_nodes ADD CONSTRAINT way_nodes_nid_fkey FOREIGN KEY (nid) REFERENCES node;");
			
			stmt.executeUpdate("CREATE TABLE way2 AS SELECT * FROM way WHERE 1=2;");
			stmt.executeUpdate("ALTER TABLE way2 ADD PRIMARY KEY (wid);");
			stmt.executeUpdate("INSERT INTO way2 (wid) (SELECT DISTINCT wid from way_nodes);");
			stmt.executeUpdate("UPDATE way2 SET highway = way.highway, cycleway = way.cycleway, bicycle = way.bicycle, oneway = way.oneway, lanes = way.lanes FROM way WHERE way2.wid = way.wid;");
			stmt.executeUpdate("ALTER TABLE way RENAME TO way3;");
			stmt.executeUpdate("ALTER TABLE way2 RENAME TO way;");
			stmt.executeUpdate("ALTER TABLE way3 RENAME TO way2;");
			stmt.executeUpdate("ALTER TABLE way_nodes DROP CONSTRAINT way_nodes_wid_fkey;");
			stmt.executeUpdate("DROP TABLE way2;");
			stmt.executeUpdate("ALTER TABLE way_nodes ADD CONSTRAINT way_nodes_wid_fkey FOREIGN KEY (wid) REFERENCES way;");
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method that gets called every time a new XML tag is found.
	 * constructs node and way objects dependent of the XML tag name.
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		if (nieuw) {
			if (!qName.equals("osm")) {
				System.err.println("Het root-tag moet \"osm\" zijn, maar is " + qName + ".");
			}
			//System.out.println("nieuw netwerk!");
			nieuw = false;
			return;
		}

		if (qName.equals("node")) {
			if (huidigeNode != null)
				System.err.println("Geneste node-tags worden genegeerd");
			else {
				node = true;
				huidigeNode = new OSM_Node();
				huidigeNode.setId(Long.parseLong(attributes.getValue("id")));
				huidigeNode.setLat(Double.parseDouble(attributes.getValue("lat")));
				huidigeNode.setLon(Double.parseDouble(attributes.getValue("lon")));
				//System.out.println("nieuwe node!");
			}
			return;
		}
		if (qName.equals("way")) {
			if (huidigeWay != null)
				System.err.println("Geneste way-tags worden genegeerd");
			else {
				way = true;
				validWay = false;
				area = false;
				huidigeWay = new OSM_Way(Integer.parseInt((attributes.getValue("id"))));
				//System.out.println("nieuwe way!");
			}
			return;
		}

		if (qName.equals("nd")) {
			if (way == true) {
				long nodeID = Long.parseLong(attributes.getValue(0));
				huidigeWayNodes.add(nodeID);
				return;
			}
		}

		if (qName.equals("tag")) {
			if (way == true) {
				if (attributes.getValue("k").equals("highway")) {
					try {
						huidigeWay.setType(Highway.valueOf(attributes.getValue("v")));
						validWay = true;
					} catch (java.lang.IllegalArgumentException e) {
						validWay = false;
					}
				}

				if ((attributes.getValue("k").equals("cycleway")) || (attributes.getValue("k").equals("cycleway:left"))
						|| (attributes.getValue("k").equals("cycleway:right"))
						|| (attributes.getValue("k").equals("cycleway:both"))) {
					try {
						huidigeWay.setCyc((Cycleway.valueOf(attributes.getValue("v"))));
					} catch (java.lang.IllegalArgumentException e) {
					}
				}

				if (attributes.getValue("k").equals("bicycle")) {
					try {
						huidigeWay.setBicyc((Bicycle.valueOf(attributes.getValue("v"))));
					} catch (java.lang.IllegalArgumentException e) {
					}
				}

				if ((attributes.getValue("k").equals("oneway")) && attributes.getValue("v").equals("yes")) {
					huidigeWay.setOneWay(true);
				}

				if ((attributes.getValue("k").equals("lanes"))) {
					huidigeWay.setNum_of_lanes((int) Float.parseFloat(attributes.getValue("v")));
				}

				if ((attributes.getValue("k").equals("area")) && (attributes.getValue("v").equals("yes"))) {
					area = true;
				}

				return;
			}
			if (node == true) {
				if (attributes.getValue("k").equals("highway") && attributes.getValue("v").equals("traffic_signals")) {
					huidigeNode.setTraffic_signals(true);
				}
			}
		}

	}

	/**
	 * Gets called when starting to parse
	 */
	public void startDocument() {
		nieuw = true;
	}

	/**
	 * Method that gets called every time a closing XML tag is found.
	 * Inserts nodes and ways in the databank dependent of the XML tag name.
	 */
	public void endElement(String uri, String localName, String qName) {
		if (qName.equals("node")) {
			node = false;
			Statement stmt;
			System.out.println("Node: "+huidigeNode.getId());
			try {
				stmt = conn.createStatement();
				stmt.executeUpdate("INSERT INTO Node (nid,lat,lon,traffic_signals) VALUES ("+huidigeNode.getId()+","+huidigeNode.getLat()+","+huidigeNode.getLon()+","+huidigeNode.isTraffic_signals()+");");
			} catch (SQLException e) {
				e.printStackTrace();
				System.exit(0);
			}
			huidigeNode = null;
		}
		if (qName.equals("way")) {
			way = false;
			if ((validWay == true) && (area == false)) {
				int size = huidigeWayNodes.size();
				Statement stmt=null;
				System.out.println("Way: "+huidigeWay.getId());
				try {
					stmt = conn.createStatement();
					OSM_Way w = huidigeWay;
					String sql = "INSERT INTO Way (wid,highway,cycleway,bicycle,oneway,lanes) VALUES ("+w.getId()+",'"+w.getType()+"','"+w.getCyc()+"','"+w.getBicyc()+"',"+w.isOneWay()+","+w.getNum_of_lanes()+");";
					//System.out.println(sql);
					stmt.executeUpdate(sql);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				for (int i = 0; i < size; i++) {
					try {
						stmt.executeUpdate("INSERT INTO Way_Nodes (nid,wid) VALUES ("+huidigeWayNodes.get(0)+","+huidigeWay.getId()+");");
					} catch (SQLException e) {
						e.printStackTrace();
						System.exit(0);
					}
					huidigeWayNodes.remove(0);
				}
			}
			huidigeWay = null;
			huidigeWayNodes = new ArrayList<Long>();
		}
	}

}

package OSMData;

import java.util.ArrayList;
import java.util.Arrays;

import Graph.Descriptor;
import Graph.Edge;
import OSM.Bicycle;
import OSM.Cycleway;
import OSM.Highway;

/**
 * 
 * An extended class of Descriptor, this class adds the OSM features
 * 
 * @author Michiel Pauwels
 *
 */

public class DescriptorOSM extends Descriptor {

	private static final long serialVersionUID = 1L;

	private Highway type;
	private Cycleway cyc;
	private Bicycle bicyc;
	private boolean OneWay;
	private int num_of_lanes;
	private boolean traffic_lights;
	
	/**
	 * This constructor sets the edge id and initializes all the features
	 * @param eid
	 */
	public DescriptorOSM(long eid){
		super(eid);
		this.setType(Highway.none);
		this.setCyc(Cycleway.none);
		this.setBicyc(Bicycle.yes);
		this.setOneWay(false);
		this.setNum_of_lanes(1);
		this.setTraffic_lights(false);
	}

	/**
	 * This constructor sets the edge and initializes all the features
	 * @param edge
	 */
	public DescriptorOSM(Edge edge) {
		super(edge);
		this.setType(Highway.none);
		this.setCyc(Cycleway.none);
		this.setBicyc(Bicycle.yes);
		this.setOneWay(false);
		this.setNum_of_lanes(1);
		this.setTraffic_lights(false);
	}
	
	/**
	 * This method returns all the features in one list
	 * Lengt | Nb_nodes | Deviation | Integral | Frequency | RoadType | Cycleway | Bicycle | Oneway | Traffic_lights | Nb_lanes
	 * @return The list of features
	 */
	public ArrayList<String> getFeatureList(){
		return new ArrayList<String>(Arrays.asList(this.getRoadlength().toString(),this.getNb_nodes().toString(),
				this.getDeviation().toString(),this.getIntegral().toString(),this.getFrequency().toString(),
				this.getType().toString(),this.getCyc().toString(),this.getBicyc().toString(),String.valueOf(this.isOneWay()),
				String.valueOf(this.isTraffic_lights()),String.valueOf(this.getNum_of_lanes())));
	}

	/**
	 * Returns a string with all the information about the descriptor
	 */
	public String toString() {
		String returntext = "";
		returntext += "Id: " + this.getId() + "\n";
		returntext += "Lengte: " + this.getRoadlength() + "\n";
		returntext += "Aantal nodes: " + this.getNb_nodes() + "\n";
		returntext += "Deviatie: " + this.getDeviation() + "\n";
		returntext += "Frequentie: " + this.getFrequency() + "\n";
		returntext += "Integraal: " + this.getIntegral() + "\n";
		returntext += "Highway: " + this.getType() + "\n";
		returntext += "Bicycle: " + this.getBicyc() + "\n";
		returntext += "Cycleway: " + this.getCyc() + "\n";
		returntext += "Eenrichting: " + this.isOneWay() + "\n";
		returntext += "Aantal lanen: " + this.getNum_of_lanes() + "\n";
		returntext += "Verkeerslichten: " + this.isTraffic_lights() + "\n\n";
		return returntext;
	}

	public Highway getType() {
		return type;
	}

	public void setType(Highway type) {
		if (type == null) {
			return;
		}
		this.type = type;
	}

	public Cycleway getCyc() {
		return cyc;
	}

	public void setCyc(Cycleway cyc) {
		if (cyc == null) {
			return;
		}
		this.cyc = cyc;
	}

	public Bicycle getBicyc() {
		return bicyc;
	}

	public void setBicyc(Bicycle bicyc) {
		if (bicyc == null) {
			return;
		}
		this.bicyc = bicyc;
	}

	public boolean isOneWay() {
		return OneWay;
	}

	public void setOneWay(boolean oneWay) {
		OneWay = oneWay;
	}

	public int getNum_of_lanes() {
		return num_of_lanes;
	}

	public void setNum_of_lanes(int num_of_lanes) {
		if (num_of_lanes == 0) {
			return;
		}
		this.num_of_lanes = num_of_lanes;
	}

	public boolean isTraffic_lights() {
		return traffic_lights;
	}

	public void setTraffic_lights(boolean traffic_lights) {
		this.traffic_lights = traffic_lights;
	}

}

package OSMData;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import OSM.Bicycle;
import OSM.Cycleway;
import OSM.Highway;
import OSM.OSM_Network;
import OSM.OSM_Node;
import OSM.OSM_Way;

/**
 * 
 * A class that parses an OSM XML InputStream into an OSM network object
 * 
 * @author Michiel Pauwels
 *
 */

public class OSM_SAXParser extends DefaultHandler {

	private SAXParser parser;
	private InputStream stroom;

	private OSM_Node huidigeNode;
	private OSM_Way huidigeWay;
	private OSM_Network network;
	private ArrayList<OSM_Node> huidigeWayNodes = new ArrayList<OSM_Node>();

	private boolean nieuw=true;
	private boolean way;
	private boolean validWay;
	private boolean area;
	private boolean node;

	/**
	 * Constructor that initializes the parser and sets the InputStream
	 * @param s The OSM InputStream
	 */
	public OSM_SAXParser(InputStream s) {
		stroom = s;
		
		try {
			parser = SAXParserFactory.newInstance().newSAXParser();
		} catch (ParserConfigurationException e) {
			System.out.println(e);
		} catch (SAXException e) {
			System.out.println(e);
		}
	}

	/**
	 * Method that parses the Inputstream into the OSM network object
	 * @return An OSM network object
	 */
	public OSM_Network leesIn() {
		long startTime = System.nanoTime();
		System.out.println("Start XML Parsing");
		try {
			parser.parse(stroom, this);
		} catch (SAXException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		long endTime = System.nanoTime();
		System.out.println("Inlezen XML: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
		return network;
	}

	/**
	 * Method that gets called every time a new XML tag is found.
	 * constructs node and way objects dependent of the XML tag name.
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		if (nieuw) {
			if (!qName.equals("osm")) {
				System.err.println("Het root-tag moet \"osm\" zijn, maar is " + qName + ".");
			}
			network = new OSM_Network();
			nieuw = false;
			return;
		}
		if (qName.equals("bounds")) {
			network.setMinlat(Double.parseDouble(attributes.getValue("minlat")));
			network.setMinlon(Double.parseDouble(attributes.getValue("minlon")));
			network.setMaxlat(Double.parseDouble(attributes.getValue("maxlat")));
			network.setMaxlon(Double.parseDouble(attributes.getValue("maxlon")));
			return;
		}

		if (qName.equals("node")) {
			if (huidigeNode != null)
				System.err.println("Geneste node-tags worden genegeerd");
			else {
				node = true;
				huidigeNode = new OSM_Node();
				huidigeNode.setId(Long.parseLong(attributes.getValue("id")));
				huidigeNode.setLat(Double.parseDouble(attributes.getValue("lat")));
				huidigeNode.setLon(Double.parseDouble(attributes.getValue("lon")));
				//System.out.println("nieuwe node!");
			}
			return;
		}
		if (qName.equals("way")) {
			if (huidigeWay != null)
				System.err.println("Geneste way-tags worden genegeerd");
			else {
				way = true;
				validWay = false;
				area = false;
				huidigeWay = new OSM_Way(Integer.parseInt((attributes.getValue("id"))));
			}
			return;
		}

		if (qName.equals("nd")) {
			if (way == true) {
				long nodeID = Long.parseLong(attributes.getValue(0));
				huidigeWayNodes.add((network.SearchNode(nodeID)));
			}
			return;
		}

		if (qName.equals("tag")) {
			if (way == true) {
				if (attributes.getValue("k").equals("highway")) {
					try {
						huidigeWay.setType(Highway.valueOf(attributes.getValue("v")));
						validWay = true;
					} catch (java.lang.IllegalArgumentException e) {
						validWay = false;
					}
				}

				if ((attributes.getValue("k").equals("cycleway")) || (attributes.getValue("k").equals("cycleway:left"))
						|| (attributes.getValue("k").equals("cycleway:right"))
						|| (attributes.getValue("k").equals("cycleway:both"))) {
					try {
						huidigeWay.setCyc((Cycleway.valueOf(attributes.getValue("v"))));
					} catch (java.lang.IllegalArgumentException e) {
					}
				}

				if (attributes.getValue("k").equals("bicycle")) {
					try {
						huidigeWay.setBicyc((Bicycle.valueOf(attributes.getValue("v"))));
					} catch (java.lang.IllegalArgumentException e) {
					}
				}

				if ((attributes.getValue("k").equals("oneway")) && attributes.getValue("v").equals("yes")) {
					huidigeWay.setOneWay(true);
				}

				if ((attributes.getValue("k").equals("lanes"))) {
					huidigeWay.setNum_of_lanes(Integer.parseInt(attributes.getValue("v")));
				}

				if ((attributes.getValue("k").equals("area")) && (attributes.getValue("v").equals("yes"))) {
					area = true;
				}

				return;
			}
			if (node == true) {
				if (attributes.getValue("k").equals("highway") && attributes.getValue("v").equals("traffic_signals")) {
					huidigeNode.setTraffic_signals(true);
				}
			}
		}

	}

	/**
	 * Gets called when starting to parse
	 */
	public void startDocument() {
		nieuw = true;
	}

	/**
	 * Method that gets called every time a closing XML tag is found.
	 * Set nodes and ways in the network object dependent of the XML tag name.
	 */
	public void endElement(String uri, String localName, String qName) {
		if (qName.equals("node")) {
			node = false;
			network.addNode((huidigeNode));
			huidigeNode = null;
		}
		if (qName.equals("way")) {
			way = false;
			if ((validWay == true) && (area == false)) {
				int size = huidigeWayNodes.size();
				for (int i = 0; i < size; i++) {
					huidigeWay.AddNode(huidigeWayNodes.get(0));
					huidigeWayNodes.remove(0);
				}
				network.addWay((huidigeWay));
			}
			huidigeWay = null;
			huidigeWayNodes = new ArrayList<OSM_Node>();
		}
	}

}

package com.kuleuven.masterthesis.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("Location")
public interface LocationService extends RemoteService{
	String sendLocationServer(String loc, int model) throws IllegalArgumentException;
}

package com.kuleuven.masterthesis.client;

import java.util.List;

import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.MapOptions;
import org.gwtopenmaps.openlayers.client.MapWidget;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.control.SelectFeature;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.ClickFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.SelectFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.UnselectFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeatureOptions;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.format.GeoJSON;
import org.gwtopenmaps.openlayers.client.layer.OSM;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * 
 * The client side page which will be compiled into JavaScript. 
 * 
 * @author Michiel Pauwels
 *
 */

public class RoadQualityMaps implements EntryPoint {
	
	private TextBox SearchBar = new TextBox();
	private Button SearchButton = new Button("Zoek");
	private FormPanel form = new FormPanel();
	private FileUpload Uploader = new FileUpload();
	
	private ListBox models = new ListBox();
	private MapWidget mapWidget;
	private SelectFeatureOptions selectOptions;
	private Vector layer;
	private int layercounter = 0;
	
	private LocationServiceAsync myService = (LocationServiceAsync) GWT.create(LocationService.class);
	
	/**
	 * This method sets all the functionality and layout of the webpage.
	 */
	public void onModuleLoad() {
		
		Label header = new Label("Road Rating Webapp");
		header.getElement().setAttribute("style", "font-size: 65px;font-family: 'Arvo';text-align: center");
		
		HorizontalPanel top = new HorizontalPanel();
		top.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		top.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		top.getElement().setAttribute("style", "width: 100%; margin: 1%;font-family: 'Arvo';");
		VerticalPanel locationwidget = getLocationWidget();
		top.add(locationwidget);
		top.add(getFileUploaderWidget());
		Button b = new Button("info", new ClickHandler() {
            public void onClick(ClickEvent event) {
                getInfo();
            }
        });
		top.add(b);
		top.setCellWidth(locationwidget, "40%");
		top.setCellWidth(form, "40%");
		top.setCellWidth(b, "20%");
		
		HorizontalPanel bottom = new HorizontalPanel();
		bottom.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		bottom.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		bottom.getElement().setAttribute("style", "margin: 1%;font-family: 'Arvo';");
		Image kuleuven = new Image("kuleuven.png");
		kuleuven.setSize("80%", "auto");
		Label samenwerking = new Label("Naar de masterthesis van Michiel Pauwels: 'A region independent road rating system based upon the Bayes classifer' in een samenwerking van KULeuven en Mobiel21");
		Image mobiel21 = new Image("mobiel21.jpg");
		mobiel21.setSize("80%","auto");
		bottom.add(kuleuven);
		bottom.add(samenwerking);
		bottom.add(mobiel21);
		bottom.setCellWidth(kuleuven, "15%");
		bottom.setCellWidth(samenwerking, "70%");
		bottom.setCellWidth(mobiel21, "15%");
		
		Panel mapPanel = new FlowPanel();
		mapPanel.add(getMap());
		mapPanel.getElement().setAttribute("style", "display: block;margin: auto; width: 100%; height: 100%;");
		Panel centerPanel = new FlowPanel();
		centerPanel.add(mapPanel);
		centerPanel.getElement().setAttribute("style", "background-color: #ffffff;");
		
		Panel Panel = new FlowPanel();
		Panel.getElement().setAttribute("style", "padding: 2%; margin-left:15%;margin-right:15%;display: inline-block;background-color: #ffffff; box-shadow: 0px 0px 25px black;");
		Panel VPanel = new VerticalPanel();
		VPanel.add(header);
		VPanel.add(top);
		VPanel.add(centerPanel);
		VPanel.add(bottom);
		Panel.add(VPanel);
		
		Document.get().getBody().setAttribute("style", "background-color: grey;");
	    RootLayoutPanel.get().add(Panel);
	   
	}
	
	/**
	 * A method that creates the mapWidget. 
	 * It sets the size, the baselayer, the center, and selectOptions
	 * @return The MapWidget
	 */
	private MapWidget getMap(){
		MapOptions defaultMapOptions = new MapOptions();
		mapWidget = new MapWidget("500px", "500px", defaultMapOptions);
		mapWidget.setWidth("100%");
		//mapWidget.setHeight("100%");
		
		OSM baselayer = OSM.Mapnik("Mapnik");   // Label for menu 'LayerSwitcher'
		baselayer.setIsBaseLayer(true);
		
		Map map = mapWidget.getMap();
	    map.addLayer(baselayer);

	    LonLat lonLat = new LonLat( 4.700376,50.879334);          
	    lonLat.transform("EPSG:4326", "EPSG:900913");      
	    map.setCenter(lonLat, 10);  
	    
	    selectOptions = new SelectFeatureOptions();
	    selectOptions.setHover();
	    selectOptions.clickFeature(new ClickFeatureListener(){
			@Override
			public void onFeatureClicked(VectorFeature vectorFeature) {
				vectorFeature.setStyle(getClickedStyle(vectorFeature));
				layer.redraw();
			}
	    });
	    selectOptions.onSelect(new SelectFeatureListener(){
			@Override
			public void onFeatureSelected(VectorFeature vectorFeature) {
				vectorFeature.setStyle(getHoveredStyle(vectorFeature));
				layer.redraw();
			}
	    	
	    });
	    selectOptions.onUnSelect(new UnselectFeatureListener(){
			@Override
			public void onFeatureUnselected(VectorFeature vectorFeature) {
				vectorFeature.setStyle(getStyle(vectorFeature));
				layer.redraw();
			}
	    });
	    
	    return mapWidget;
	}

	/**
	 * A method that gets called when clicked on the search button.
	 * It adds a waiting gif, calls a server method via RPC, and waits for the answer
	 * On failure, it shows the failed message.
	 * On succes, it creates a new styled layer and displays it
	 */
	private void searchLocation() {
		String text = SearchBar.getText()+";"+mapWidget.getOffsetHeight()+";"+mapWidget.getOffsetWidth();
		int model = models.getSelectedIndex();
		setWaitingGif();
		myService.sendLocationServer(text,model, new AsyncCallback<String>() {

        public void onFailure(Throwable caught) {
        	RootLayoutPanel.get().remove(1);
        	popupMessage(caught.getMessage());
        }

		public void onSuccess(String geojson) {
			RootLayoutPanel.get().remove(1);
			int zoom = Integer.parseInt(geojson.split(";")[0]);
			geojson = geojson.split(";")[1];
			GeoJSON json = new GeoJSON();
			layer = new Vector("Roadscores"+layercounter);
			layercounter++;
			VectorFeature[] features = json.read(geojson);
			for(int i=0; i<features.length; i++){
				features[i].setStyle(getStyle(features[i]));
			}
			layer.addFeatures(features);
			
			Map map = mapWidget.getMap();
			final SelectFeature select = new SelectFeature(layer,selectOptions);
			select.setAutoActivate(true);
			map.addControl(select);
			map.removeOverlayLayers();
			
			map.addLayer(layer);
			Bounds bounds = layer.getDataExtent();
			map.setCenter(bounds.getCenterLonLat(),zoom+1);
			map.setRestrictedExtent(bounds);
			map.zoomIn();
			map.zoomOut();
			map.setMinMaxZoomLevel(zoom+1, 19);
		}
      });
		
	}
	
	/**
	 * A method that creates the location widget
	 * @return A vertical panel with a Searchbar and a button
	 */
	private VerticalPanel getLocationWidget(){
		VerticalPanel Zoek = new VerticalPanel();
		Zoek.add(new Label("Zoek op locatie: "));
		HorizontalPanel scontrols = new HorizontalPanel();
		models.addItem("Routecoach model");
		models.addItem("Gebruikersmodel");
		scontrols.add(SearchBar);
		scontrols.add(models);
		scontrols.add(SearchButton);
		SearchButton.addClickHandler(new ClickHandler() {
		      public void onClick(ClickEvent event) {
		          searchLocation();
		        }
		      });
		Zoek.add(scontrols);
		return Zoek;
	}
	
	/**
	 * A method that creates the uploader widget
	 * @return A Form widget with a FileUploader and an upload button
	 */
	private FormPanel getFileUploaderWidget() {
		
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
        form.setMethod(FormPanel.METHOD_POST);
        form.setAction(GWT.getModuleBaseURL()+"fileupload");
        
        VerticalPanel Upload = new VerticalPanel();
		Upload.add(new Label("Upload zelf fietsgegevens (.gpx)"));
        HorizontalPanel holder = new HorizontalPanel();
        Upload.add(holder);

        Uploader.setName("upload");
        holder.add(Uploader);
        holder.add(new Button("Submit", new ClickHandler() {
            public void onClick(ClickEvent event) {
                form.submit();
            }
        }));

        form.addSubmitHandler(new FormPanel.SubmitHandler() {
            public void onSubmit(SubmitEvent event) {
                if ("".equalsIgnoreCase(Uploader.getFilename())) {
                    event.cancel();
                    popupMessage("No file was selected");
                }
            }
        });

        form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            public void onSubmitComplete(SubmitCompleteEvent event) {
            	if(event.getResults().contains("upload_size_exceeded")){popupMessage("Het bestand overschrijdt de maximale grootte...");}
            	else if(event.getResults().contains("fileuploadexception")){popupMessage("Er ging iets mis bij het uploaden van het bestand..");}
            	else if(event.getResults().contains("wrong_extension")){popupMessage("Het bestand bevat geen .gpx extensie..");}
            	else {popupMessage("Upload voltooid!");}
            }
        });

        form.add(Upload);
        //form.getElement().setAttribute("style", "float:right");
        return form;
    }
	
	/**
	 * A method that generates a style based upon the class of the vectorfeature
	 * @param vf The vectorfeature
	 * @return a style object
	 */
	private Style getStyle(VectorFeature vf){
		
		int strokewidth = 6;
		double opacity = 0.5;
		
		Style vfstyle = new Style();
		vfstyle.setStrokeWidth(strokewidth);
		vfstyle.setStrokeOpacity(opacity);
		
		String sclass = vf.getAttributes().getAttributeAsString("class");
		if(sclass.equals("very_bad")){vfstyle.setStrokeColor("red");}
		else if(sclass.equals("bad")){vfstyle.setStrokeColor("orange");}
		else if(sclass.equals("moderate")){vfstyle.setStrokeColor("yellow");}
		else if(sclass.equals("good")){vfstyle.setStrokeColor("GreenYellow");}
		else if(sclass.equals("very_good")){vfstyle.setStrokeColor("green");}
		else {vfstyle.setStrokeColor("black");}
		return vfstyle;
	}
	
	/**
	 * A method that generates a style based upon the class of the vectorfeature
	 * @param vf The vectorfeature
	 * @return a style object
	 */
	private Style getHoveredStyle(VectorFeature vf){
		int strokewidth = 8;
		double opacity = 0.4;

		Style vfcstyle = getStyle(vf);
		vfcstyle.setStrokeWidth(strokewidth);
		vfcstyle.setStrokeOpacity(opacity);
		
		return vfcstyle;
	}
	
	private Style getClickedStyle(VectorFeature vf){
		String data = "";
		List<String> attrnames = vf.getAttributes().getAttributeNames();
		for(int i=0; i<attrnames.size(); i++){
			data += attrnames.get(i)+" : "+vf.getAttributes().getAttributeAsString(attrnames.get(i))+"\n";
		}
		Style vfcstyle = getHoveredStyle(vf);
		vfcstyle.setLabel(data);
		return vfcstyle;
	}
	
	/**
	 * A method that creates a custom pop up message
	 * @param message
	 */
	public static void popupMessage(String message){
		PopupPanel msg = new PopupPanel();
		VerticalPanel content = new VerticalPanel();
		msg.getElement().setAttribute("style", "display: block; margin: auto; width: 15%; height: 15%;");
		content.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		content.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		content.add(new Label(message));
		content.add(new Button("Sluit",new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootLayoutPanel.get().remove(1);
            }
        }));
		content.getElement().setAttribute("style", "margin: 5px auto;");
		content.setSpacing(20);
		msg.add(content);
		msg.getElement().getStyle().setZIndex(1000);
		RootLayoutPanel.get().add(msg);
	}
	
	/**
	 * Adds a popup panel with a waiting gif to the rootlayoutpanel
	 */
	private void setWaitingGif(){
		PopupPanel msg = new PopupPanel();
		VerticalPanel content = new VerticalPanel();
		msg.getElement().setAttribute("style", "display: block; margin: auto; width: 20%; height: 20%;");
		content.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		content.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		Image image = new Image("waitinggif.gif");
		image.getElement().setAttribute("style", "display: block;margin: auto; width:100%;");
		content.add(image);
		msg.add(content);
		msg.getElement().getStyle().setZIndex(1000);
		RootLayoutPanel.get().add(msg);
	}
	
	/**
	 * Adds a popup panel with the application info to the rootlayoutpanel
	 */
	public static void getInfo(){
		PopupPanel msg = new PopupPanel();
		VerticalPanel content = new VerticalPanel();
		msg.getElement().setAttribute("style", "display: block; margin: auto; width: 45%; height: 70%; text-align:left;");
		content.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		content.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		HTML text = new HTML("<h2>Een regio onafhankelijk systeem dat wegen beoordeeld op fietsvriendelijkheid</h2>Deze applicatie is het resultaat van een masterthesis van een student Industri&euml;le ingenieurswetenschappen ICT. "
				+ "De webapplicatie beoordeeld wegen op fietsvriendelijkheid op basis van een getraind model. Hiervoor werd het "
				+ "<a href=\"https://en.wikipedia.org/wiki/Naive_Bayes_classifier\">naive Bayes</a> model gebruikt. Dit model is getraind op "
				+ "een aantal weg eigenschappen, die verkregen zijn via <a href=\"http://wiki.openstreetmap.org/wiki/Key:highway\">OpenStreetMap</a>, "
				+ "en berekende scores van deze wegen op basis van gebruikersdata. Deze data werd verkregen via de <a href=\"http://www.routecoach.be/\">"
				+ "Routecoach app</a>. Ook is het mogelijk om een eigen GPX bestand met fietsgegevens te uploaden en zelf een model te helpen trainen. "
				+ "Op deze manier worden twee modellen getraind. Wanneer er een locatie ingegeven wordt, zal er een kaart gegenereerd worden op basis "
				+ "van &eacute;&eacute;n van de twee modellen. Op deze kaart is met een kleurcode de score aangegeven. Wanneer er met de cursor over een weg gegaan wordt,"
				+ "worden de eigenschappen van deze weg weergegeven alsook de bijdrages van deze eigenschappen tot het bekomen van de uiteindelijke score."
				+ "Ook wordt de score (class) weergegeven. Op deze manier kan er gekeken worden welke wegen fietsvriendelijk zijn en welke niet."
				+ "De wegeigenschappen worden hier nog eens kort uitgelegd: "
				+ "<ul>"
				+ "<li>Lengte : de effectieve lengte van het stuk weg</li>"
				+ "<li>Aantal nodes : het aantal shapepoints en kruispunten van het stuk weg</li>"
				+ "<li>Deviatie : de gemiddelde afwijking van een rechte door de kruispunten</li>"
				+ "<li>Integraal : de oppervlakte ten opzichte van een rechte door de twee kruispunten</li>"
				+ "<li>Frequentie : de frequentie die het sterkste voorkomt in de vorm van de weg </li>"
				+ "<li>Highway type (OSM) : bepaalt over welk type weg het gaat (pedestrian, residential, primary,...)</li>"
				+ "<li>Cycleway (OSM) : meer uitleg over het fietspad (gescheiden fietspad,...)</li>"
				+ "<li>Bicycle road (OSM) : geeft aan of er een apart fietspad is</li>"
				+ "<li>Oneway (OSM) : geeft true voor een eenrichtingsstraat</li>"
				+ "<li>Lanes (OSM) : het aantal rijlanen van de weg</li>"
				+ "<li>Traffic signals (OSM) : geeft true indien er op het stuk weg een verkeerslicht voorkomt</li>"
				+ "</ul>");
		text.getElement().setAttribute("style", "height: 80%; width:100%;overflow: auto;");
		content.add(text);
		Button button = new Button("Sluit",new ClickHandler() {
            public void onClick(ClickEvent event) {
            	RootLayoutPanel.get().remove(1);
            }
        });
		content.add(button);
		content.setCellHorizontalAlignment(button, HasHorizontalAlignment.ALIGN_CENTER);
		content.getElement().setAttribute("style", "margin: 5px auto;");
		content.setSpacing(20);
		msg.add(content);
		msg.getElement().getStyle().setZIndex(1000);
		RootLayoutPanel.get().add(msg);
	}

}

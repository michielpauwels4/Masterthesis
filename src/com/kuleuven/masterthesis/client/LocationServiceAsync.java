package com.kuleuven.masterthesis.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface LocationServiceAsync {
	void sendLocationServer(String input, int model, AsyncCallback<String> asyncCallback) throws IllegalArgumentException;
}

package com.kuleuven.masterthesis.server;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * 
 * A class which get http requests with gpx files
 * 
 * @author Michiel Pauwels
 *
 */

public class FileUploader extends HttpServlet{

	private static final long serialVersionUID = 1L;
	private String dir = "UserData/";
	
	/**
	 * This method gets called every time a http post request gets send.
	 * It checks if the extension is '.gpx' and if the file size is not too big.
	 * If both are good then the file is saved in the right directory, and 
	 * a new modeltrain thread is started.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
		
		System.out.println("start fileUpload!");
		ServletFileUpload upload = new ServletFileUpload();
		
		int filenumber = getMaxFileNumber()+1;

        try{
            FileItemIterator iter = upload.getItemIterator(request);

            while (iter.hasNext()) {
                FileItemStream item = iter.next();

                String name = item.getName();
                if(!(name.split("\\.")[1]).equals("gpx")){throw new IllegalArgumentException();}
                System.out.println("Filename: "+name);
                InputStream stream = item.openStream();
                
                int maxFileSize = 10*(1024*1024); //10 megs max
                int len = 0, totlen = 0;
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(dir + "GPX"+filenumber+".gpx")));
                byte[] buffer = new byte[8192];
                while ((len = stream.read(buffer, 0, buffer.length)) != -1) {
                	if(totlen >= maxFileSize){
                		bos.close();
                		(new File("UserData/GPX"+filenumber+".gpx")).delete();
                		throw new IOException("File is larger than " + maxFileSize);
                	}
                	bos.write(buffer, 0, len);
                	totlen += len;
                }
                bos.close();
                filenumber++;
            }
            System.out.println("FileUpload Complete!");
        } catch(IllegalArgumentException e){
        	response.getWriter().write("wrong_extension");
        	return;
	    } catch(IOException e){
        	response.getWriter().write("upload_size_exceeded");
        	return;
        } catch (FileUploadException e) {
        	response.getWriter().write("fileuploadexception"); 
        	return;
		}
        (new Thread(new ModelTrainThread(dir,6000,0))).start();
	}
	
	private int getMaxFileNumber(){
		File[] files = new File(dir).listFiles();
		int maxNumber = 0;
		for(int i = 0; i < files.length; i++){
			if(!files[i].getPath().contains(".gpx")){files[i].delete(); continue;}
			System.out.println(files[i].getPath());
			String[] FileNumber = (files[i].getPath().split(".gpx")[0]).split("GPX");
			System.out.println(FileNumber[1]);
			int number = Integer.parseInt(FileNumber[1]);
			if(number > maxNumber) maxNumber = number;
		}
		return maxNumber;
	}

}

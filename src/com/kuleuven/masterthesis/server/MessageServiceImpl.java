package com.kuleuven.masterthesis.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.kuleuven.masterthesis.client.LocationService;

import BayesClassifier.BayesClassifier;
import Graph.Descriptor;
import Graph.Edge;
import Graph.Network;
import OSMData.DescriptorOSM;
import OSMData.NetworkFetcher;

/**
 * 
 * A class that handles RPC communication. 
 * 
 * @author Michiel Pauwels
 *
 */

public class MessageServiceImpl extends RemoteServiceServlet implements LocationService {

	private static final long serialVersionUID = 1L;
	
	public static String modelFileName = "models.dat";
	public static String dBName;
	public static String Username;
	public static String Password;
	
	private long fileDate;
	private ArrayList<BayesClassifier> models;
	private HashMap<String,CacheUnit> cachedStrings;
	
	@SuppressWarnings("unchecked")
	/**
	 * This method gets called the first time an RPC comes in. It initializes the model and the cache hashmap.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public MessageServiceImpl() throws IOException {
		System.out.println("Opstarten Server!");
		
		cachedStrings = new HashMap<String,CacheUnit>();
		
		File f = new File(modelFileName);
		fileDate = f.lastModified();
		
		System.out.println((new File(".")).getCanonicalPath());
		
		try {
			 BufferedReader dbconfig = new BufferedReader(new FileReader("dbconfig"));
			 dBName = dbconfig.readLine().split(":")[1].trim();
			 Username = dbconfig.readLine().split(":")[1].trim();
			 Password = dbconfig.readLine().split(":")[1].trim();
			 dbconfig.close();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		} 
		
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(modelFileName));
			models = (ArrayList<BayesClassifier>) ois.readObject();
			System.out.println("file founded and read in!");
		} catch (FileNotFoundException e) {
			System.out.println("no classifiers found");
			models = null;
		} catch (ClassNotFoundException e) {
			System.out.println("class was not recognized");
			models = null;
		} finally {
			if(ois!=null) ois.close();
		}
		
	}

	@Override
	/**
	 * The method that gets called from the client side. It looks if the location is in cache, 
	 * before fetching a whole network. If the object is not in cache, the actual method gets called.
	 * @return The GeoJSON String
	 */
	public String sendLocationServer(String loc, int model) throws IllegalArgumentException {
		
		System.out.println("start serverside script!");
		System.out.println(loc);
		
		String cachestring = model+";"+loc;

		if(!cachedStrings.containsKey(cachestring)){
			if(cachedStrings.size()>20){
				cleanCache();
			}
			cachedStrings.put(cachestring, new CacheUnit(this.rateRoads(loc,model)));
		}
		return (String)cachedStrings.get(cachestring).getObject();
	}
	
	/**
	 * The method that determines a new map layer
	 * It uses geocoding to convert a location string to a bounding box,
	 * which is used to fetch the right OSM network. This network gets fitted on the model and put in a GeoJSON String.
	 * @param loc
	 * @return
	 */
	private String rateRoads(String loc, int model){
		
		try {
			checkNewModel();
		} catch (IOException e1) {}
		
		String[] parts = loc.split(";");
		loc = parts[0].replaceAll(" ", "%20");
		
		System.out.println("Locatie: "+parts[0]+" , MapVerhouding: "+parts[1]+","+parts[2]+" , modelnummer: "+model);
		
		Document doc;
		try {
			URL website = new URL("https://maps.googleapis.com/maps/api/geocode/xml?address="+loc+",Belgie&key=AIzaSyCVDrAhuAe1RsAlzZJFkGI0Jfqsx17TEfY");
			InputStream stream = website.openStream();
			
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    doc = dBuilder.parse(stream);
		    
		    stream.close();
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("De locatie die u probeerde te vinden bevat verkeerde tekens");
		} catch (IOException e) {
			return null;
		} catch (ParserConfigurationException e) {
			return null;
		} catch (SAXException e) {
			return null;
		}
		
		System.out.println("geocoding succeeded!");
		float lat,lon,maxlat,minlat;
		try{
			NodeList location = doc.getElementsByTagName("location").item(0).getChildNodes();
			lat = Float.parseFloat(location.item(1).getTextContent());
			lon = Float.parseFloat(location.item(3).getTextContent());
			maxlat = Float.parseFloat(doc.getElementsByTagName("northeast").item(0).getChildNodes().item(1).getTextContent());
			minlat = Float.parseFloat(doc.getElementsByTagName("southwest").item(0).getChildNodes().item(1).getTextContent());
		} catch(NullPointerException e){
			throw new IllegalArgumentException("De locatie die u opgaf had geen resultaten");
		}
		int zoom = (int) (Math.round(Math.log(360/Math.abs(maxlat-minlat))/Math.log(2)))+1;
		if(zoom < 12){throw new IllegalArgumentException("De locatie die u probeerde te zoeken is te groot om te verwerken");}
		else if(zoom < 14){zoom = 14;}
		else if(zoom > 17){zoom = zoom - 2;}
		else if(zoom == 17){zoom--;}
		System.out.println("lat: "+lat+", lon: "+lon+", zoom: "+zoom);
		
	    ArrayList<Float>bounds = this.CheckBounds(lat,lon, zoom ,5000, (Double.parseDouble(parts[1])/(Double.parseDouble(parts[2]))));

	    NetworkFetcher nf = new NetworkFetcher(bounds.get(0),bounds.get(1),bounds.get(2),bounds.get(3));
	    Network network;
		try {
			network = nf.FetchNetworkDB();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Er is een probleem met de connectie naar de databank");
		}
		
		if(network.getArcs().size()==0){throw new IllegalArgumentException("De locatie die u opgaf had geen resultaten");}
	    
	    String GeoJSON = zoom+";{"
	    		+ "\t\"type\": \"FeatureCollection\","
	    		+ "\t\"features\": [";
	    boolean komma = false;
	    for(int i=0; i<network.getArcs().size(); i++){
	    	if(komma){GeoJSON += ",\n";}
	    	komma=true;
	    	try {
				String klasse = models.get(model).getBestClassComp(((DescriptorOSM)network.getArcs().get(i).getDescriptor()).getFeatureList());
				ArrayList<Double> likelihoods = models.get(model).calculateCompLikelihoods(klasse, ((DescriptorOSM)network.getArcs().get(i).getDescriptor()).getFeatureList());
				GeoJSON += this.EdgeToJson(network.getArcs().get(i), klasse, likelihoods);
	    	} catch (NullPointerException | IndexOutOfBoundsException e){
	    		throw new IllegalArgumentException("Het model dat u probeerde te gebruiken is niet beschikbaar");
	    	}
			
	    }
	    return GeoJSON + "]}";
	}
	
	/**
	 * A method that checks if a new model was trained. If there is a new model,
	 * it gets read in.
	 * @throws IOException 
	 */
	@SuppressWarnings("unchecked")
	private void checkNewModel() throws IOException {
		File f = new File(modelFileName);
		if(fileDate != f.lastModified()){
			System.out.println("Updating to a new model!");
			fileDate = f.lastModified();
			cachedStrings = new HashMap<String,CacheUnit>();
			ObjectInputStream ois = null;
			try {
				ois = new ObjectInputStream(new FileInputStream(modelFileName));
				models = (ArrayList<BayesClassifier>) ois.readObject();
				System.out.println("file founded and read in!");
			} catch (FileNotFoundException | ClassCastException | ClassNotFoundException e) {
				System.out.println("no classifiers found");
				models = null;
			} finally {
				if(ois!=null) ois.close();
			}
		}
	}

	/**
	 * A method which converts an edge with a known class to a piece of GeoJSON syntax.
	 * @param e The edge 
	 * @param eclass The best fitted class for the edge
	 * @param likelihoods 
	 * @return A piece of GeoJSON
	 */
	private String EdgeToJson(Edge e, String eclass, ArrayList<Double> likelihoods){
		
		if(eclass.equals("")){eclass = "unknown";}
		Descriptor desc = e.getDescriptor();
		
		String json = "\t\t{\n"
				+ "\t\t\t\"type\": \"Feature\",\n"
				+ "\t\t\t\"geometry\": {\n"
				+ "\t\t\t\t\"type\": \"LineString\",\n"
				+ "\t\t\t\t\"coordinates\": [";
		ArrayList<Double> lonLat = transform(e.getA().getLat(),e.getA().getLon());
		json += "["+lonLat.get(0)+","+lonLat.get(1)+"]";
		for(int i=0; i<e.getPath().size(); i++){
			lonLat = transform(e.getPath().get(i).getLat(),e.getPath().get(i).getLon());
			json += ",["+lonLat.get(0)+","+lonLat.get(1)+"]"; 
		}
		lonLat = transform(e.getB().getLat(),e.getB().getLon());
		json += ",["+lonLat.get(0)+","+lonLat.get(1)+"]";
		json	+=	"]\n"
				+ "\t\t\t},\n"
				+ "\t\t\t\"properties\": {\n"
				+ "\t\t\t\t\"Lengte\": \""+(int)(desc.getLength())+" ("+Math.round(likelihoods.get(0))+"%)\",\n"
				+ "\t\t\t\t\"Aantal nodes\": \""+desc.getNb_nodes()+" ("+Math.round(likelihoods.get(1))+"%)\",\n"
				+ "\t\t\t\t\"Deviatie\": \""+desc.getDeviation()+" ("+Math.round(likelihoods.get(2))+"%)\",\n"
				+ "\t\t\t\t\"Frequentie\": \""+desc.getFrequency()+" ("+Math.round(likelihoods.get(3))+"%)\",\n"
				+ "\t\t\t\t\"Integraal\": \""+desc.getIntegral()+" ("+Math.round(likelihoods.get(4))+"%)\",\n" 
				+ "\t\t\t\t\"Highway type\": \""+((DescriptorOSM)desc).getType()+" ("+Math.round(likelihoods.get(5))+"%)\",\n"
				+ "\t\t\t\t\"Cycleway\": \""+((DescriptorOSM)desc).getCyc()+" ("+Math.round(likelihoods.get(6))+"%)\",\n"
				+ "\t\t\t\t\"Bicycle\": \""+((DescriptorOSM)desc).getBicyc()+" ("+Math.round(likelihoods.get(7))+"%)\",\n"
				+ "\t\t\t\t\"Oneway\": \""+((DescriptorOSM)desc).isOneWay()+" ("+Math.round(likelihoods.get(8))+"%)\",\n"
				+ "\t\t\t\t\"Traffic signals\": \""+((DescriptorOSM)desc).isTraffic_lights()+" ("+Math.round(likelihoods.get(9))+"%)\",\n"
				+ "\t\t\t\t\"Lanes\": \""+((DescriptorOSM)desc).getNum_of_lanes()+" ("+Math.round(likelihoods.get(10))+"%)\",\n"
				+ "\t\t\t\t\"class\": \""+eclass+"\"\n"
				+ "\t\t\t}\n" 
				+ "\t\t}";
		
		//System.out.println(json);
		return json;
	}
	
	private void cleanCache() {
		Iterator<String> it = cachedStrings.keySet().iterator();
		int minhits = Integer.MAX_VALUE;
		String smallestkey = null;
		while(it.hasNext()){
			String key = it.next();
			int hits = cachedStrings.get(key).nb_hits;
			if(hits<minhits){
				minhits = hits;
				smallestkey = key;
			}
			cachedStrings.get(key).nb_hits = hits/2;
		}
		cachedStrings.remove(smallestkey);
	}

	/**
	 * 
	 * @param lat
	 * @param lon
	 * @param zoom
	 * @param maxWindow
	 * @param ratio
	 * @return
	 */
	private ArrayList<Float> CheckBounds(float lat,float lon, int zoom, float maxWindow, double ratio){
		
		double xlon = (360.0/Math.pow(2, zoom))*1.25;
		double xlat = xlon*(ratio*0.63);

		ArrayList<Float> bounds = new ArrayList<Float>();
		bounds.add((float) (lat-xlat));
		bounds.add((float) (lat+xlat));
		bounds.add((float) (lon-xlon));
		bounds.add((float) (lon+xlon));
		return bounds;
	}
	
	/**
	 * A method that transforms coordinates from 'EPSG:4326' to 'EPSG:900913'
	 * @param lat The latitude from the 'EPSG:4326' coordinate system
	 * @param lon The longitude from the 'EPSG:4326' coordinate system
	 * @return A set of coordinates in the 'EPSG:900913' coordinate system
	 */
	private ArrayList<Double> transform(double lat, double lon){
		ArrayList<Double> mercator = new ArrayList<Double>();
		double x = lon * 20037508.34 / 180.0;
		double y = Math.log(Math.tan((90.0 + lat) * Math.PI / 360.0)) / (Math.PI / 180.0);
		y = y * 20037508.34 / 180.0;
		mercator.add(x); mercator.add(y);
		return mercator;
	}

}

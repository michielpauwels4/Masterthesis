package com.kuleuven.masterthesis.server;

/**
 * 
 * A class which is used to store and count cached objects
 * 
 * @author Michiel Pauwels
 *
 */

public class CacheUnit {
	
	public int nb_hits;
	private Object object;
	
	/**
	 * The constructor of the CacheUnit. 
	 * @param o The object that will be stored
	 */
	public CacheUnit(Object o){
		object = o;
		nb_hits = 0;
	}
	
	/**
	 * While getting the object, the number of hits increase
	 * @return The object
	 */
	public Object getObject(){
		nb_hits++;
		return object;
	}
}

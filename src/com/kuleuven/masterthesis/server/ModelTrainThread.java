package com.kuleuven.masterthesis.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import BayesClassifier.BayesClassifier;
import Graph.Edge;
import Graph.Network;
import MapMatching.RoadRater;
import MapMatching.RoadScore;
import OSMData.DescriptorOSM;
import OSMData.NetworkFetcher;
import UserData.GPSPoint;
import UserData.Trip;
import UserData.UserDataFetcher;

/**
 * 
 * A class which trains a model based on GPX files in a separate thread
 * 
 * @author Michiel Pauwels
 *
 */

public class ModelTrainThread implements Runnable {
	
	private String directory;
	private int window;
	private int mindata;
	
	/**
	 * The constructor which sets the directory of the gpx files,
	 * the maximal clusterdiameter and the minimal data for the RoadRater
	 * @param dir The directory of the gpx files
	 * @param w The maximal clusterdiameter
	 * @param mdata The minimal amount of data needed
	 */
	public ModelTrainThread(String dir,int w, int mdata){
		directory = dir;
		window = w;
		mindata = mdata;
	}

	@SuppressWarnings("unchecked")
	@Override
	/**
	 * A method which trains the model.
	 * It reads every gpx file into a list of trips and clusters the data.
	 * For each cluster, a network is fetched and matched to the data and the roads get rated.
	 * The model then gets trained and written to a file.
	 */
	public void run(){
		long startTime = System.nanoTime();
		String data = "";
		UserDataFetcher uf = new UserDataFetcher(directory+".gpx",window);
        ArrayList<ArrayList<GPSPoint>> clusters = uf.getUserData();
        ArrayList<Trip> tripsl = uf.trips;
        for(int i=0; i<clusters.size(); i++){
        	try {
				NetworkFetcher nf = new NetworkFetcher(clusters.get(i),tripsl);
				RoadRater rr = new RoadRater(nf.FetchNetworkDB(),nf.trips,mindata);
				rr.RateRoads();
				data += rr.data;

			} catch(Exception e){
				e.printStackTrace();
			}
        }
        
        PrintWriter out;
		try {
			out = new PrintWriter("data.txt");
			out.write(data);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		ArrayList<Long> wids = new ArrayList<Long>();
		ArrayList<Integer> been = new ArrayList<Integer>();
		ArrayList<Integer> avoided = new ArrayList<Integer>();
		ArrayList<Double> scores = new ArrayList<Double>();
		int count = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(new File("data.txt")))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       String parts[] = line.split(";");
		       wids.add(Long.parseLong(parts[0]));
		       been.add(Integer.parseInt(parts[1]));
		       avoided.add(Integer.parseInt(parts[2]));
		       count++;
		    }
		}  catch (IOException e) {
			e.printStackTrace();
		}
		BayesClassifier bayes = new BayesClassifier("GPXModel",11);
		NetworkFetcher nf = new NetworkFetcher(0,0,0,0);
		Network n = nf.getNetworkFromIDsDB(wids);
		int beensum = 0;
		int avoidsum = 0;
		int mindata = 5;
		
		for(int i=0; i<wids.size(); i++){
			if(been.get(i) + avoided.get(i) > mindata){
				beensum += been.get(i);
				avoidsum += avoided.get(i);
				double score = (double)(been.get(i)+1)/((double)(been.get(i)+1)+(double)(avoided.get(i)+1));
				scores.add(score);
			}
		}
		
		for(int i=0; i<count; i++){
			if(been.get(i) + avoided.get(i) > mindata){
				beensum += been.get(i);
				avoidsum += avoided.get(i);
				double score = (double)(been.get(i)+1)/((double)(been.get(i)+1)+(double)(avoided.get(i)+1));
				scores.add(score);
			}
		}
		scores.sort(new Comparator<Double>(){

			@Override
			public int compare(Double arg0, Double arg1) {
				return arg0.compareTo(arg1);
			}
			
		});
		double tresh1 = scores.get(scores.size()/5);
		double tresh2 = scores.get(2*scores.size()/5);
		double tresh3 = scores.get(3*scores.size()/5);
		double tresh4 = scores.get(4*scores.size()/5);
		double ratio = (double)beensum/(double)avoidsum;
		ratio = 1;
		System.out.println("ratio: "+ratio);
		System.out.println("tresh1: "+tresh1+", tresh2: "+tresh2+", tresh3: "+tresh3+", tresh4: "+tresh4);
		System.out.println("aantal opgehaalde edges: "+n.getArcs().size());
		for(int i=0; i<count; i++){
			if(been.get(i) + avoided.get(i) > mindata){
				double score = (double)(been.get(i)+1)/((double)(been.get(i)+1)+(double)(avoided.get(i)+1));
				
				RoadScore scoreclass;
				if(score < tresh1){scoreclass = RoadScore.very_bad;}
				else if(score < tresh2){scoreclass = RoadScore.bad;}
				else if(score < tresh3){scoreclass = RoadScore.moderate;}
				else if(score < tresh4){scoreclass = RoadScore.good;}
				else {scoreclass = RoadScore.very_good;}
				
				DescriptorOSM desc = (DescriptorOSM) CheckEdge(n,wids.get(i)).getDescriptor();
				bayes.Learn(desc.getFeatureList(), scoreclass.toString());
			}
		}
		
		bayes.write2file("model");
		
		String modelname = MessageServiceImpl.modelFileName;		
		ArrayList<BayesClassifier> models;
		
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(modelname));
			models = (ArrayList<BayesClassifier>) ois.readObject();
		} catch (ClassCastException | ClassNotFoundException | IOException e) {
			models = null;
		} finally {
			if(ois!=null)
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
		if(models != null){
			for(int i=0; i<models.size();i++){if(models.get(i).getModelname().equals("GPXModel")) models.remove(i);}
			models.add(bayes);
		}
		else {
			models = new ArrayList<BayesClassifier>();
			models.add(bayes);
		}
		
		ObjectOutputStream outo;
		try {
			outo = new ObjectOutputStream(new FileOutputStream(modelname));
			outo.writeObject(models);
			outo.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		(new File(modelname)).setLastModified((new Date()).getTime());
		
		long endTime = System.nanoTime();
		System.out.println("Total time training GPX model: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
	}
	
	/**
	 * Searches for an edge in a network given the OSM id
	 * @param n A network object
	 * @param wid An OSM id
	 * @return An edge if found, NULL if not found
	 */
	private Edge CheckEdge(Network n, long wid){
		for(int i=0; i<n.getArcs().size(); i++){
			if(n.getArcs().get(i).getOSMid()==wid){return n.getArcs().get(i);}
		}
		return null;
	}
}

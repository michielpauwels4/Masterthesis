package MapMatching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import Graph.Descriptor;
import Graph.Edge;
import Graph.Network;
import UserData.GPSPoint;
import UserData.Trip;

/**
 * 
 * 
 * 
 * @author Michiel Pauwels
 *
 */

public class RoadRater {
	
	private Network network;
	private ArrayList<Trip> trips;
	private HashMap<Long, Integer> been = new HashMap<Long, Integer>();
	private HashMap<Long, Integer> avoided = new HashMap<Long, Integer>();
	public HashMap<Long, RoadScore> scores = new HashMap<Long, RoadScore>();
	public HashMap<Long, Descriptor> descriptors = new HashMap<Long, Descriptor>();
	public String data = "";
	private int minData;
	
	public RoadRater(Network n,  ArrayList<Trip> t, int min){
		network = n;
		trips = t;
		minData = min;
		cutRegion();
	}
	
	public void cutRegion() {
		int splitcounter = 0;
		//verwijdert elk punt dat niet binnen window van het netwerk past
		for (int i = 0; i < trips.size(); i++) {
			Trip t = trips.get(i);
			boolean out = false, split = false;
			Trip newTrip = null;
			for (int k = 0; k < t.getRoute().size(); k++) {
				GPSPoint current = t.getRoute().get(k);
				if (isOut(current) && out == true) {
					split = true;
					out = true;
					t.getRoute().remove(k);
					k--;
				} else if (isOut(current) && out == false) {
					out = true;
					t.getRoute().remove(k);
					k--;
					if (split) {
						trips.add(newTrip);
						newTrip = null;
					}
					split = true;
				} else if (!isOut(current) && out == true) {
					t.getRoute().remove(k);
					k--;
					out = false;
					newTrip = new Trip();
					newTrip.getRoute().add(current);
					newTrip.setStart(current.getTimestamp());
					newTrip.setEnd(current.getTimestamp());
				} else if (!isOut(current) && out == false && split) {
					t.getRoute().remove(k);
					k--;
					out = false;
					newTrip.getRoute().add(current);
					newTrip.setEnd(current.getTimestamp());
				} else if (!isOut(current) && out == false && !split) {
					t.setEnd(current.getTimestamp());
				}
			}
			if (newTrip != null)
				trips.add(newTrip);
			if (trips.get(i).getRoute().size() == 0) {
				trips.remove(i);
				i--;
			}
			if (split)
				splitcounter++;
		}
		System.out.println(splitcounter + " trips gesplitst!");
		System.out.println("Na verwijderen/splitsen nog " + trips.size() + " trips over!");
	}

	public boolean isOut(GPSPoint p) {
		if (p.getLat() < network.getMinlat() || p.getLat() > network.getMaxlat() || p.getLon() < network.getMinlon()
				|| p.getLon() > network.getMaxlon()) {
			return true;
		} else {
			return false;
		}
	}

	public void RateRoads(){
		if(network==null){return;}
		MapMatcher mm = new MapMatcher(network);
		ShorterRoutes sr = new ShorterRoutes(new ShortestPath());
		//int counter = 0;
		for(int i=0; i<trips.size(); i++){
			//System.out.println("trip nummer: "+(i+1));
			//counter++;
			try {
				MatchedTrip trip = mm.matchTrip(trips.get(i));
				updateNumbers(trip.matchedRoute,sr.calcShorterRoutes(trip));
			} catch(NullPointerException e){
				System.out.println("trip gefaald: "+trips.get(i).getRoute().size());
				//System.exit(0);
				//System.out.println("matchen niet gelukt");
				//counter--;
			}
		}
		Iterator<Long> iterator = been.keySet().iterator();
	    while(iterator.hasNext()) {
	    	long beenKey = iterator.next();
	        if(avoided.containsKey(beenKey)){
	        	calculateScore(beenKey,been.get(beenKey),avoided.get(beenKey));
	        	avoided.remove(beenKey);
	        }
	        else {
	        	calculateScore(beenKey,been.get(beenKey),0);
	        }
	    }
	    iterator = avoided.keySet().iterator();
	    while(iterator.hasNext()) {
	    	long Key = iterator.next();
	    	calculateScore(Key,0,avoided.get(Key));
	    }
	    iterator = scores.keySet().iterator();
	    while(iterator.hasNext()) {
	    	long Key = iterator.next();
	    	//TODO: nework vind descriptor van edge niet altijd!
	    	descriptors.put(Key, network.CheckEdge(Key).getDescriptor());
	    }
	}
	
	public void updateNumbers(ArrayList<Edge> matched, ArrayList<Edge> avoid){
		for(int i=0; i<matched.size(); i++){
			if(network.CheckEdge(matched.get(i).getId()) == null){System.out.println(matched.get(i).getId()+" bestaat niet in netwerk!");}
			if(been.containsKey(matched.get(i).getId())){
				been.put(matched.get(i).getId(), been.get(matched.get(i).getId())+1);
			}
			else {
				been.put(matched.get(i).getId(), 1);
			}
		}
		for(int i=0; i<avoid.size(); i++){
			if(network.CheckEdge(avoid.get(i).getId()) == null){System.out.println("Bestaat niet in netwerk!");}
			if(avoided.containsKey(avoid.get(i).getId())){
				avoided.put(avoid.get(i).getId(), avoided.get(avoid.get(i).getId())+1);
			}
			else {
				avoided.put(avoid.get(i).getId(), 1);
			}
		}
	}
	
	public void calculateScore(long id, int been, int avoided){
		
		double weight = minData + 1;
		
		if((been+avoided)<minData) return;
		double score = (double)(been+1)/((double)(been+1)+(double)((avoided+1)*weight));
		if(Double.isNaN(score)){return;}
		
			//System.out.println("score = "+score);
		if(score<0.2){scores.put(id,RoadScore.very_bad);}
		else if(score < 0.4){scores.put(id, RoadScore.bad);}
		else if(score < 0.6){scores.put(id,RoadScore.moderate);}
		else if(score < 0.8){scores.put(id,RoadScore.good);}
		else {scores.put(id,RoadScore.very_good);}
		
		data += network.CheckEdge(id).getOSMid()+";"+been+";"+avoided+"\r\n";
		
//		String scores = been+";"+avoided+"\r\n";
//		try {
//		    Files.write(Paths.get("scores.txt"), scores.getBytes(), StandardOpenOption.APPEND);
//		}catch (IOException e) {
//		    //exception handling left as an exercise for the reader
//		}
	}
	
}
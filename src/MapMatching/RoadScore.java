package MapMatching;

public enum RoadScore {
	very_bad, bad, moderate, good, very_good, no_score
}

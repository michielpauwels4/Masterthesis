package MapMatching;

import java.util.ArrayList;

import Graph.Edge;
import Graph.Node;

/**
 * 
 * A simple class consisting of a start and end node, and a list of edges.
 * 
 * @author Michiel Pauwels
 *
 */

public class MatchedTrip {
	
	protected Node startN;
	protected Node endN;
	protected ArrayList<Edge> matchedRoute;
	
	public MatchedTrip(ArrayList<Edge> trip){
		matchedRoute = trip;
	}
}

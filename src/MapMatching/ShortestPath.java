package MapMatching;

import java.util.ArrayList;
import java.util.Arrays;
import Graph.Edge;
import Graph.Node;

/**
 * 
 * A class which computes the shortest path based on the A* algorithm
 * 
 * @author Michiel Pauwels
 *
 */

public class ShortestPath {
	
	/**
	 * The A* algorithm which computes the shortest path from a startNode to an endNode.
	 * @param startNode
	 * @param endNode
	 * @return A list of edges which is the shortest path
	 */
	public ArrayList<Edge> Astar(Node startNode, Node endNode) {

		if (startNode.equals(endNode)) {
			return new ArrayList<Edge>();
		}

		ArrayList<Node> closedSet = new ArrayList<Node>();
		ArrayList<Node> openSet = new ArrayList<Node>(Arrays.asList(startNode));
		ArrayList<Edge> neighbours;
		Edge neighbour;
		Node huidig, buur;
		double gScore;
		this.initialiseAstar(startNode, endNode);
		while (openSet.size() != 0) {
			huidig = this.getSmallestAstar(openSet);
			if (huidig.equals(endNode)) {
				return this.reconstruct_path(huidig, startNode);
			}
			openSet.remove(huidig);
			closedSet.add(huidig);
			neighbours = huidig.getEdges();
			for (int i = 0; i < neighbours.size(); i++) {
				neighbour = neighbours.get(i);
				buur = getNeighbour(huidig, neighbour);
				if (closedSet.contains(buur)) {
					continue;
				}
				gScore = huidig.getWeight() + neighbour.getLength();
				if (!openSet.contains(buur)) {
					this.initialize(buur, endNode);
					openSet.add(buur);
				} else if (gScore >= buur.getWeight()) {
					continue;
				}
				buur.setBefore(huidig);
				buur.setWeight(gScore);
				buur.setfScore(gScore + this.heuristicCostEstimate(buur, endNode));
			}
		}
		return new ArrayList<Edge>();
	}
	
	private void initialiseAstar(Node startN, Node endN) {
		startN.setWeight(0);
		startN.setfScore(this.heuristicCostEstimate(startN, endN));
		startN.setBefore(null);
	}

	private Node getSmallestAstar(ArrayList<Node> list) {
		int index = 0;
		double minimum = Double.POSITIVE_INFINITY;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getfScore() < minimum) {
				index = i;
				minimum = list.get(i).getfScore();
			}
		}
		return list.get(index);
	}
	
	private Node getNeighbour(Node n, Edge e) {
		if (e.getA().equals(n)) {
			return e.getB();
		} else {
			return e.getA();
		}
	}

	private void initialize(Node n, Node endN) {
		n.setWeight(Double.POSITIVE_INFINITY);
		n.setfScore(Double.POSITIVE_INFINITY);
		n.setBefore(null);
	}

	private double heuristicCostEstimate(Node startN, Node endN) {
		return startN.CalculateDist(endN);
	}

	

	private ArrayList<Edge> reconstruct_path(Node endN, Node startN) {
		ArrayList<Edge> path = new ArrayList<Edge>();
		Node curr = endN, prev;
		while (!curr.equals(startN)) {
			prev = curr.getBefore();
			path.add(0, curr.getEdge(prev));
			curr = prev;
		}
		return path;
	}
	
}

package MapMatching;

import java.util.ArrayList;

import Graph.Edge;
import Graph.Node;

/**
 * 
 * A class which computes the routes which are shorter than the trip, 
 * and returns a list of shorter but avoided edges
 * 
 * @author Michiel Pauwels
 *
 */

public class ShorterRoutes {

	private ArrayList<Edge> shorter;
	private ShortestPath sp;
	
	/**
	 * The constructor for this class. Sets the ShortestPath class and initializes the shorter ArrayList
	 * @param astar The ShortestPath class, which uses the A* algorithm
	 */
	public ShorterRoutes(ShortestPath astar){
		sp = astar;
		shorter = new ArrayList<Edge>();
	}

	/**
	 * A method which computes the routes which are shorter than the trip, 
     * and returns a list of shorter but avoided edges.
	 * @param trip
	 * @return The list of shorter but avoided edges
	 */
	public ArrayList<Edge> calcShorterRoutes(MatchedTrip trip) {
		ArrayList<Edge> korter = new ArrayList<Edge>();
		Node huidigeN = trip.startN, eindN = trip.endN;
		for (int i = 0; i < trip.matchedRoute.size() - 1; i++) {
			if (huidigeN == null) {
				huidigeN = trip.matchedRoute.get(i).getCommonNode(trip.matchedRoute.get(i + 1));
				continue;
			}
			korter = sp.Astar(huidigeN, eindN);
			this.addRoutes(korter);
			huidigeN = trip.matchedRoute.get(i).getCommonNode(trip.matchedRoute.get(i + 1));
		}
		return korter;
	}

	private void addRoutes(ArrayList<Edge> korter) {
		for (int i = 0; i < korter.size(); i++) {
			if (!shorter.contains(korter.get(i))) {
				shorter.add(korter.get(i));
			}
		}
	}

}
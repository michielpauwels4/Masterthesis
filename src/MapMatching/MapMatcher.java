package MapMatching;

import java.util.ArrayList;
import java.util.Arrays;
import Graph.*;
import UserData.GPSPoint;
import UserData.Trip;

/**
 * 
 * A topological map matching algorithm with good accuracy
 * 
 * @author Michiel Pauwels
 *
 */

public class MapMatcher {
	
	private ArrayList<Trip> trips;
	private Trip trip;
	private Network network;
	private ShortestPath sp = new ShortestPath();
	private ArrayList<Edge> matchedEdges;
	private int linkedPoints;
	private Node ClosestN;

	/**
	 * The constructor for map matching a single trip
	 * @param network The routable network
	 */
	public MapMatcher(Network network){
		this.network = network;
	}

	/**
	 * The constructor for matching a list of trips at once
	 * @param network The routable network
	 * @param trips The list of trips
	 */
	public MapMatcher(Network network, ArrayList<Trip> trips) {
		System.out.println("Start Mapmatching");
		long startTime = System.nanoTime();
		this.network = network;
		this.trips = trips;
		this.Match();
		long endTime = System.nanoTime();
		System.out.println("Map Matching: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
	}

	/**
	 * Matches the entire list
	 * @return Returns a list of MatchedTrip objects
	 * @throws NullPointerException Thrown if the trips where not set
	 */
	public ArrayList<MatchedTrip> Match() throws NullPointerException {
		if(trips == null){throw new NullPointerException();}
		ArrayList<MatchedTrip> matchedtrips = new ArrayList<MatchedTrip>();
		int badcounter = 0;
		for (int i = 0; i < trips.size(); i++) {
			Trip trip = trips.get(i);
			try {
				matchedtrips.add(this.matchTrip(trip));
			} catch (Exception e) {
				badcounter++;
			}
		}
		System.out.println(badcounter + " trips NIET gematcht");
		return matchedtrips;
	}

	/**
	 * This method matches a trip to a set of edges. First it gets the edge closest to the first point, 
	 * and matches all points in the proximity of that edge. It then chooses a new edge out of a set of candidates 
	 * made of the neighbor edges of the previous matched edge. For every candidate edge it calculates a score and 
	 * chooses the edge with the highest score to be matched and so on until every point has been matched.
	 * @param trip A trip to be matched
	 * @return A matchedTrip object
	 */
	public MatchedTrip matchTrip(Trip trip){
		ArrayList<Edge> ways = new ArrayList<Edge>(Arrays.asList(matchInit(trip.getRoute().get(0))));
		ArrayList<GPSPoint> points, chosenPoints = null;
		double score, maxScore;
		int index;
		this.trip = trip;
		matchedEdges = new ArrayList<Edge>();
		linkedPoints = 0;
		ClosestN = null;

		while (linkedPoints < trip.getRoute().size()) {
			index = -1;
			maxScore = 0;
			for (int i = 0; i < ways.size(); i++) {
				Edge way = ways.get(i);
				points = this.getPoints(way);
				score = this.calculateScore(points, way);
				if (score > maxScore) {
					maxScore = score;
					chosenPoints = points;
					index = i;
				}
			}
			if (index == -1 && ClosestN == null){
				this.matchPoints(chosenPoints, ways.get(0));
			}
			else if (index == -1) {
				noPoints();

			} else {
				this.matchPoints(chosenPoints, ways.get(index));
			}
			ways = ClosestN.getEdges();
		}
		
		return constructMatchedTrip();
		
	}
	
	/**
	 * Finds the edge in closest proximity to a certain GPSPoint
	 * @param gpsPoint
	 * @return The closest edge
	 */
	private Edge matchInit(GPSPoint gpsPoint) {
		double mindistance = Double.POSITIVE_INFINITY;
		Edge closest = null;
		for (int i = 0; i < network.getArcs().size(); i++) {
			for (int u = 0; u < network.getArcs().get(i).getPath().size(); u++) {
				ShapePoint k = network.getArcs().get(i).getPath().get(u);
				if (mindistance > gpsPoint.CalculateDist(k)) {
					mindistance = gpsPoint.CalculateDist(k);
					closest = network.getArcs().get(i);
				}
			}
		}
		return closest;
	}
	
	/**
	 * Determines a set of gps-points which are likely to be on a way
	 * @param way The candidate way
	 * @return A set of gps-points
	 */
	private ArrayList<GPSPoint> getPoints(Edge way) {
		ArrayList<GPSPoint> lijst = new ArrayList<GPSPoint>();
		boolean start = false;
		int minDistance = 0;
		for (int i = 0; i < way.getPath().size(); i++) {
			if (this.DistanceToLink(way, way.getPath().get(i)) > minDistance) {
				minDistance = (int) this.DistanceToLink(way, way.getPath().get(i));
			}
		}
		minDistance += 30;
		if (linkedPoints == 0) {
			lijst.add(trip.getRoute().get(0));
		}
		for (int i = linkedPoints; i < trip.getRoute().size(); i++) {
			if ((linkedPoints == 0) && (i == 0))
				continue;
			GPSPoint huidig = trip.getRoute().get(i);
			double angleA = way.getA().getIncludedAngle(huidig, way.getB());
			double angleB = way.getB().getIncludedAngle(huidig, way.getA());
			if ((angleA < Math.PI / 2) && (angleB < Math.PI / 2) && (this.DistanceToLink(way, huidig) <= minDistance)) {
				start = true;
				lijst.add(huidig);
			} else {
				if (start || this.DistanceToLink(way, huidig) > minDistance) {
					break;
				} else {
					lijst.add(huidig);
				}
			}
		}
		return lijst;
	}
	
	/**
	 * Calculates a score for a way based on a set of gps points
	 * @param points The points in the proximity of the way
	 * @param way The candidate way
	 * @return A score for the candidate way
	 */
	private double calculateScore(ArrayList<GPSPoint> points, Edge way) {
		double WS, W, D, score = 0;
		for (int i = 0; i < points.size(); i++) {
			GPSPoint huidig = points.get(i);
			if ((ClosestN == null))
				W = 1;
			else
				W = this.CalcModus(ClosestN, way, huidig, 30);
			D = Math.pow(this.DistanceToLink(way, huidig), 2);
			WS = 100 * W / (D + 1);
			if (Double.isNaN(WS)) {
				WS = 0;
			}
			score += WS;
		}
		return score;
	}
	
	private double CalcModus(Node node, Edge way, GPSPoint huidig, double perimeter) {
		double distance = Math
				.sqrt(Math.pow(huidig.CalculateDist(node), 2) - Math.pow(this.DistanceToLink(way, huidig), 2));
		if (distance > perimeter)
			return distance / perimeter;
		else
			return (-Math.cos((distance * Math.PI) / perimeter) + 1) / 2.0;
	}
	
	/**
	 * Determines the closest edge to the new unlinked GPS point. 
	 * It then determines the closest node from the closest edge, and computes the A* algorithm 
	 * from the current node to the node from the closest edge. The edges from the A* algorithm
	 * are added to the matched edges.
	 */
	private void noPoints() {
		double min = Double.POSITIVE_INFINITY;
		Node dicht = null, tweede = null;
		Edge kortste = null;
		GPSPoint nieuwste = trip.getRoute().get(linkedPoints);
		for (int i = 0; i < network.getArcs().size(); i++) {
			if (this.DistanceToLink(network.getArcs().get(i), nieuwste) < min) {
				min = this.DistanceToLink(network.getArcs().get(i), nieuwste);
				kortste = network.getArcs().get(i);
			}
		}
		if (trip.getRoute().get(linkedPoints).CalculateDist(kortste.getA()) < trip.getRoute().get(linkedPoints).CalculateDist(kortste.getB())) {
			dicht = kortste.getA();
			tweede = kortste.getB();
		} else {
			dicht = kortste.getB();
			tweede = kortste.getA();
		}
		ArrayList<Edge> path = sp.Astar(ClosestN, dicht);
		if (path.size() == 0) {
			path = sp.Astar(ClosestN, tweede);
			dicht = tweede;
		}
		path.forEach((edge) -> {
			matchedEdges.add(edge);
		});
		ClosestN = dicht;
		if (this.getPoints(kortste).size() > 0)
			this.matchPoints(this.getPoints(kortste), kortste);
		else {
			ArrayList<GPSPoint> lijst = new ArrayList<GPSPoint>();
			lijst.add(trip.getRoute().get(linkedPoints));
			this.matchPoints(lijst, kortste);
		}
	}

	/**
	 * The linkedPoints are incremented by the number of chosen points.
	 * The matched way is added to the list of matched ways.
	 * @param chosenPoints The points matched to the new way
	 * @param way The matched way
	 */
	private void matchPoints(ArrayList<GPSPoint> chosenPoints, Edge way) {
		linkedPoints += chosenPoints.size();
		if ((ClosestN == null) && (linkedPoints < trip.getRoute().size())) {
			if (trip.getRoute().get(linkedPoints).CalculateDist(way.getA()) < trip.getRoute().get(linkedPoints).CalculateDist(way.getB())) {
				ClosestN = way.getA();
			} else
				ClosestN = way.getB();
		} else {
			ClosestN = way.otherNode(ClosestN);
		}
		matchedEdges.add(way);
	}
	
	/**
	 * The list of edges is converted to a matchedTrip object
	 * @return a matchedTrip object
	 */
	private MatchedTrip constructMatchedTrip(){
		if (matchedEdges.size() < 2) {
			return new MatchedTrip(new ArrayList<Edge>());
		}
		for (int i = 0; i < matchedEdges.size() - 1; i++) {
			if (matchedEdges.get(i).equals(matchedEdges.get(i + 1))) {
				matchedEdges.remove(i + 1);
				i--;
			}
		}
		MatchedTrip newMatched = new MatchedTrip(matchedEdges);
		newMatched.startN = matchedEdges.get(0).otherNode(matchedEdges.get(0).getCommonNode(matchedEdges.get(1)));
		newMatched.endN = matchedEdges.get(matchedEdges.size() - 1).otherNode(matchedEdges.get(matchedEdges.size() - 1).getCommonNode(matchedEdges.get(matchedEdges.size() - 2)));
		
		this.matchedEdges.remove(0);
		this.matchedEdges.remove(this.matchedEdges.size() - 1);
		
		newMatched.matchedRoute = this.matchedEdges;
		return newMatched;
	}

	/**
	 * @param way
	 * @param huidig
	 * @return The distance from the GPS point to the way
	 */
	private double DistanceToLink(Edge way, GPSData huidig) {
		double alpha1 = way.getA().getIncludedAngle(way.getB(), huidig);
		double alpha2 = way.getB().getIncludedAngle(way.getA(), huidig);
		if (alpha2 > Math.PI / 2 || alpha1 > Math.PI / 2) {
			if (way.getA().CalculateDist(huidig) < way.getB().CalculateDist(huidig)) {
				return way.getA().CalculateDist(huidig);
			} else {
				return way.getB().CalculateDist(huidig);
			}
		} else {
			return (way.getA().CalculateDist(huidig) * Math.sin(alpha1)
					+ way.getB().CalculateDist(huidig) * Math.sin(alpha2)) / 2.0;
		}
	}

}
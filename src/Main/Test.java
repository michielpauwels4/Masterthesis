package Main;

import java.sql.SQLException;
import java.util.ArrayList;

import OSMData.NetworkFetcher;
import UserData.GPSPoint;
import UserData.Trip;
import UserData.UserDataFetcher;

/**
 * 
 * A Testclass used to get timing results from several tests
 * 
 * @author Michiel Pauwels
 *
 */

public class Test {

	public static void main(String[] args) {
		UserDataFetcher uf = new UserDataFetcher("UserDataCopy.csv",5000);
		ArrayList<ArrayList<GPSPoint>> clusters = uf.getUserData();
		ArrayList<Trip> tripsl = uf.trips;
		long startTime = System.nanoTime();
		for(int i=0; i<clusters.size(); i++){
			long startTime2 = System.nanoTime();
			NetworkFetcher nf = new NetworkFetcher(clusters.get(i),tripsl);
			try {
				//nf.FetchNetwork();
				nf.FetchNetworkDB();
			} /* catch (InterruptedException e) {
				e.printStackTrace();
			}*/ catch (SQLException e) {
				e.printStackTrace();
			}
			long endTime2 = System.nanoTime();
			System.out.println("Total time cluster network: " + Math.round(((endTime2 - startTime2) * (Math.pow(10.0, -9.0)))) + "sec");
		}
		long endTime = System.nanoTime();
		System.out.println("Total time network: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
	}

}

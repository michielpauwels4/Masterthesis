package BayesClassifier;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * The frequency table of a feature from the Bayes classifier.
 * 
 * @author Michiel Pauwels
 *
 */

public class FrequencyTable implements Serializable{
	
	private static final long serialVersionUID = 1L;
	protected List<List<Integer>> dynamicTable = new ArrayList<List<Integer>>();
	protected List<String> reference = new ArrayList<String>();
	private int nb_rows=0;
	protected int nb_cols=0;
	
	private void addRow(){
		ArrayList<Integer> newRow = new ArrayList<Integer>();
		for(int i=0; i<nb_cols; i++){
			newRow.add(1);
		}
		dynamicTable.add(newRow);
	}
	
	private void addColumn(){
		for(int i=0; i<nb_rows; i++){
			dynamicTable.get(i).add(1);
		}
	}
	
	private double calculateClass(int classindex){
		double returnv = 0;
		for(int i=0; i<nb_rows; i++){
			returnv += dynamicTable.get(i).get(classindex);
		}
		return returnv;
	}
	
	/**
	 * Adjusts the frequency table by incrementing the right value
	 * @param feature The row index
	 * @param classindex The column index
	 */
	protected void addElement(String feature, int classindex){
		if(!reference.contains(feature)){
			nb_rows++;
			reference.add(feature);
			addRow();
		}
		if(classindex>=nb_cols){
			nb_cols++;
			addColumn();
		}
		dynamicTable.get(reference.indexOf(feature)).set(classindex, dynamicTable.get(reference.indexOf(feature)).get(classindex)+1);
		
	}
	
	/**
	 * Calculates the likelihood of a given feature and class
	 * @param feature
	 * @param classindex The columnindex of a certain class
	 * @return The likelihood of a given feature and class
	 */
	protected double calculateLikelihood(String feature, int classindex){
		if(reference.indexOf(feature)==-1 || classindex>=nb_cols ) return 1.0;
		return ((double)dynamicTable.get(reference.indexOf(feature)).get(classindex))/calculateClass(classindex);
	}
	
	/**
	 * Calculates the complement likelihood of a given feature and class
	 * @param feature
	 * @param classindex The columnindex of a certain class
	 * @return The complement likelihood of a given feature and class
	 */
	protected double calcComplementLikelihood(String feature, int classindex){
		double occurence = 0;
		double classoccurence = 0;
		for(int i=0; i<nb_cols; i++){
			if(i!=classindex){
				if(reference.indexOf(feature)!=-1 && classindex < nb_cols) occurence += dynamicTable.get(reference.indexOf(feature)).get(i);
				else {occurence++;}
				classoccurence += calculateClass(i);
				
			}
		}
		return classoccurence/occurence;
	}
	
	
	
	
}
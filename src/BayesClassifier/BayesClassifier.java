package BayesClassifier;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * The Java implementation of a Naive Bayes Classifier.
 * 
 * @author Michiel Pauwels
 *
 */

public class BayesClassifier implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private ArrayList<FrequencyTable> ftables = new ArrayList<FrequencyTable>();
	private ArrayList<String> classes = new ArrayList<String>();
	private ArrayList<Integer> classesfreq = new ArrayList<Integer>();
	private String modelname;
	private int totalcount=0;

	/**
	 *  The constructor of this class. The number of features has to be specified in advance.
	 *  @param model The name of the classifier
	 *  @param nb_feat The number of trainable features
	 */
	public BayesClassifier(String model, int nb_feat){
		modelname = model;
		for(int i=0; i<nb_feat; i++){
			ftables.add(new FrequencyTable());
		}
	}
	
	/**
	 * This method trains the classifier by adjusting the frequencytable of every feature.
	 * @param features The list of features to be trained
	 * @param fclass The given class
	 * @throws IllegalArgumentException Thrown if the number of features does not match the list size
	 */
	public void Learn(ArrayList<String> features, String fclass) throws IllegalArgumentException {
		if(features.size()!=ftables.size()){throw new IllegalArgumentException();}
		else {
			totalcount++;
			if(!classes.contains(fclass)){
				classes.add(fclass);
				classesfreq.add(0);
			}
			int classindex = classes.indexOf(fclass);
			classesfreq.set(classindex, classesfreq.get(classindex)+1);
			for(int i=0; i<ftables.size(); i++){
				ftables.get(i).addElement(features.get(i), classindex);
			}
		}
	}
	
	private double calculateChance(String fclass, ArrayList<String> features) throws IllegalArgumentException{
		if(features.size()!=ftables.size()){throw new IllegalArgumentException();}
		int classindex = classes.indexOf(fclass);
		double likelihood = 1;
		double prior = ((double)classesfreq.get(classindex))/((double)totalcount);
		for(int i=0; i<ftables.size(); i++){
			//System.out.println("feature: "+features.get(i));
			likelihood = likelihood*ftables.get(i).calculateLikelihood(features.get(i), classindex);
		}
		return likelihood*prior;
	}
	
	private double calculateCompChance(String fclass, ArrayList<String> features) throws IllegalArgumentException {
		if(features.size()!=ftables.size()){throw new IllegalArgumentException();}
		int classindex = classes.indexOf(fclass);
		double likelihood = 1;
		double prior = ((double)classesfreq.get(classindex))/((double)totalcount);
		for(int i=0; i<ftables.size(); i++){
			likelihood = likelihood*ftables.get(i).calcComplementLikelihood(features.get(i), classindex);
		}
		return prior*likelihood;
	}
	
	/**
	 * This method calculates a complement likelihood for each feature given a certain class
	 * @param fclass The given class
	 * @param features The list of features
	 * @return A set of likelihoods, one for each feature
	 * @throws IllegalArgumentException Thrown if the list size does not match the feature size
	 */
	public ArrayList<Double> calculateCompLikelihoods(String fclass, ArrayList<String> features) throws IllegalArgumentException {
		if(features.size()!=ftables.size()){throw new IllegalArgumentException();}
		ArrayList<Double> list = new ArrayList<Double>();
		for(int i=0; i<ftables.size(); i++){
			list.add((1.0-(1.0/ftables.get(i).calcComplementLikelihood(features.get(i), classes.indexOf(fclass))))*100.0);
		}
		return list;
	}
	
	/**
	 * This method calculates a likelihood for each feature given a certain class
	 * @param fclass The given class
	 * @param features The list of features
	 * @return A set of likelihoods, one for each feature
	 * @throws IllegalArgumentException Thrown if the list size does not match the feature size
	 */
	public ArrayList<Double> calculateLikelihoods(String fclass, ArrayList<String> features) throws IllegalArgumentException {
		if(features.size()!=ftables.size()){throw new IllegalArgumentException();}
		ArrayList<Double> list = new ArrayList<Double>();
		for(int i=0; i<ftables.size(); i++){
			list.add(ftables.get(i).calculateLikelihood(features.get(i), classes.indexOf(fclass)));
		}
		return list;
	}
	
	/**
	 * Calculates the best class by calculating the probability for each class and taking the maximum probability.
	 * @param features The set of given features
	 * @return The best fitted class
	 */
	public String getBestClass(ArrayList<String> features){
		String max = "";
		double maxChance = 0;
		double chance;
		for(int i=0; i<classes.size(); i++){
			chance = calculateChance(classes.get(i),features);
			if(chance >= maxChance){
				maxChance = chance;
				max = classes.get(i);
			}
		}
		return max;
	}
	
	/**
	 * Calculates the best class by calculating the complement probability for each class and taking the maximum probability.
	 * @param features The set of given features
	 * @return The best fitted class
	 */
	public String getBestClassComp(ArrayList<String> features){
		String max = "";
		double maxChance = 0;
		double chance;
		for(int i=0; i<classes.size(); i++){
			chance = calculateCompChance(classes.get(i),features);
			if(chance >= maxChance){
				maxChance = chance;
				max = classes.get(i);
			}
		}
		return max;
	}

	/**
	 * Writes a model to a CSV file
	 * @param fileName The name of the file
	 */
	public void write2file(String fileName){
		String string = modelname;
		String newLine = ";";
		for(int i=0; i<classes.size(); i++){newLine += ";";}
		newLine += "\r\n";
		string += newLine;
		for(int i=0; i<ftables.size(); i++){
			FrequencyTable ftable = ftables.get(i);
			string += ";";
			for(int u=0; u<classes.size(); u++){
				string += classes.get(u)+";";
			}
			string += "\r\n";
			for(int u=0; u<ftable.reference.size(); u++){
				string += ftable.reference.get(u)+";";
				for(int x=0; x<ftable.nb_cols; x++){
					string += ftable.dynamicTable.get(u).get(x)+";";
				}
				string += "\r\n";
			}
			string += newLine;
		}
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(fileName+".csv"));
			writer.write (string);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return Returns the name of the model
	 */
	public String getModelname() {
		return modelname;
	}
}

package UserData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * The TXT implementation of the UserDataFacade
 * 
 * @author Michiel Pauwels
 *
 */

public class TXTReader extends UserDataFacade {

	public TXTReader(String fileName) {
		super(fileName);
	}

	@Override
	/**
	 * Creates BufferedReader, reads file line per line. 
	 * For each line, the method calls ReadUserData to process the information
	 */
	public void ReadLines() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(this.getFileName()));
			String line = null;
			boolean first = true;
			int secondsPerLog = 1;
			int counter = secondsPerLog;
			while ((line = br.readLine()) != null) {
				if (counter == secondsPerLog) {
					counter = 0;
					if (first == true) {
						first = false;
					} else {
						String[] gesplitst = line.split(",");
						String time = gesplitst[0];
						time = time.substring(0, 8) + "0" + time.charAt(8) + " " + time.substring(11, time.length() - 1)
								+ ".000";
						String[] data = { "1", "POINT(" + gesplitst[2] + " " + gesplitst[1] + ")", time, time, time,
								"1", "manual" };
						//System.out.println("Punt toegevoegd: "+data[1]);
						if (Float.parseFloat(gesplitst[4]) <= 100) {
							this.ReadUserData(data);
						}
					}

				}
				counter++;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

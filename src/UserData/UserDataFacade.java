package UserData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

/**
 * 
 * A class which can be inherited to create a Reader class
 * It reads user data into trip objects
 * 
 * @author danny
 *
 */

public abstract class UserDataFacade {

	private String fileName;
	protected ArrayList<Trip> trips = new ArrayList<Trip>();
	
	/**
	 * Constructor that sets the filename
	 * @param fileName
	 */
	public UserDataFacade(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * Method that reads userdata.
	 * It then sorts the data based on timestamp, 
	 * and filters the data
	 */
	public void getUserData(){
		ReadLines();
		SorteerData();
		FilterData();
	}

	/**
	 *  Reads the data and calls ReadUserData to process it
	 *  Data: userid | location | time | starttime | stoptime | tripid | operation_mode
	 */
	protected abstract void ReadLines();

	/**
	 * Puts the data in trip objects
	 * @param line information obtained
	 */
	protected void ReadUserData(String[] line) {
		boolean trip;
		double lat = 0;
		double lon = 0;
		String time = "";
		String start = "";
		String stop = "";
		String t_id = "";
		String position[] = { "", "" };
		try {
			position[0] = line[1].substring(6, line[1].length() - 1);
			position = position[0].split(" ");
			lon = Double.parseDouble(position[0]);
			lat = Double.parseDouble(position[1]);
			time = line[2].substring(0, line[2].length() - 3);
			start = line[3].substring(0, line[3].length() - 3);
			stop = line[4].substring(0, line[4].length() - 3);
			t_id = line[5];
		} catch (Exception e) {
			e.printStackTrace();
		}

		trip = false;
		for (int u = 0; u < this.getTrips().size(); u++) {
			if (this.getTrips().get(u).getT_id().equals(t_id)) {
				trip = true;
				GPSPoint newPoint = new GPSPoint(lat, lon, time);
				this.getTrips().get(u).getRoute().add(newPoint);
			}
		}
		if (trip == false) {
			Trip newTrip = new Trip(t_id, start, stop);
			this.getTrips().add(newTrip);
			GPSPoint newPoint = new GPSPoint(lat, lon, time);
			newTrip.getRoute().add(newPoint);
		}
	}

	public String toString() {
		int gpscount = 0;
		for (int i = 0; i < this.getTrips().size(); i++) {
			gpscount += this.getTrips().get(i).getRoute().size();
		}
		return "Er zijn " + this.getTrips().size() + " trips en " + gpscount + " coordinaten ingelezen!";
	}

	/**
	 * Sorts all trips base on timestamp
	 */
	private void SorteerData() {
		for (int i = 0; i < this.getTrips().size(); i++) {
			Trip trip = this.getTrips().get(i);
			trip.getRoute().sort(new Comparator<GPSPoint>() {
				@Override
				public int compare(GPSPoint een, GPSPoint twee) {
					return een.getTimestamp().compareTo(twee.getTimestamp());
				}
			});
		}
	}

	/**
	 * Filters data based on minimum distance, minimum time, and logfrequency
	 */
	private void FilterData() {
		boolean gesorteerd = true;
		int falsecounter = 0, uitschieter = 0;
		double totaltime, secondsperlog;
		for (int i = 0; i < this.getTrips().size(); i++) {
			Trip trip = this.getTrips().get(i);
			totaltime = (trip.getEnd().getTime() - trip.getStart().getTime()) / 1000.0;
			secondsperlog = totaltime / (double) (trip.getRoute().size());
			trip.setSecondsPerPoint(secondsperlog);
			double afstand = 0;
			double tottijd = 0;
			for (int u = 0; u < trip.getRoute().size() - 1; u++) {
				GPSPoint huidig = trip.getRoute().get(u);
				GPSPoint volgend = trip.getRoute().get(u + 1);
				if (u == 0 && getVelocity(huidig, volgend) > 15 && trip.getRoute().size() > 2) {
					if (getVelocity(volgend, trip.getRoute().get(u + 2)) > 15) {
						this.getTrips().get(i).getRoute().remove(volgend);
						uitschieter++;
						u--;
					} else {
						this.getTrips().get(i).getRoute().remove(huidig);
						uitschieter++;
						u--;
					}
				} else if (getVelocity(huidig, volgend) > 15) {
					this.getTrips().get(i).getRoute().remove(volgend);
					uitschieter++;
					u--;
				} else {
					afstand += huidig.CalculateDist(volgend);
					if (this.getTime(huidig, volgend) < 0) {
						gesorteerd = false;
					}
					tottijd += this.getTime(huidig, volgend);
				}

			}
			if ((afstand < 50) || (tottijd < 60) || (trip.getSecondsPerPoint() > 17)) { //pas deftige trip vanaf 50m en lengte van minstens 1 minuut
				falsecounter++;
				this.getTrips().remove(i);
				i--;
			}

		}
		System.out.println("Gesorteerd: " + gesorteerd);
		System.out.println(
				"In totaal " + falsecounter + " slechte trips en " + uitschieter + " uitschieters verwijderd!");
		System.out.println("Er zijn in totaal nog " + this.getTrips().size() + " trips over!");
	}

	private double getVelocity(GPSPoint een, GPSPoint twee) {
		return een.CalculateDist(twee) / getTime(een, twee);
	}

	private double getTime(GPSPoint een, GPSPoint twee) {
		Date date1 = een.getTimestamp();
		Date date2 = twee.getTimestamp();
		long diff = date2.getTime() - date1.getTime();
		return ((double) diff / (double) 1000);
	}


	public String getFileName() {
		return fileName;
	}

	public ArrayList<Trip> getTrips() {
		return trips;
	}
}
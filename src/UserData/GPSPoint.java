package UserData;

import java.text.SimpleDateFormat;
import java.util.Date;

import Graph.GPSData;

/**
 * 
 * A class which hold the information for a single user datapoint.
 * Extends the GPSData class.
 * 
 * @author Michiel Pauwels
 *
 */

public class GPSPoint extends GPSData {

	private static final long serialVersionUID = 1L;
	private Date timestamp;

	/**
	 * A constructor method which sets the latitude and longitude.
	 * @param lat
	 * @param lon
	 */
	public GPSPoint(double lat, double lon) {
		super(lat, lon);
	}

	/**
	 * A constructor method which sets the latitue and longitude
	 * and parses and sets the date
	 * @param lat
	 * @param lon
	 * @param time A date String in the "yyyy-MM-dd HH:mm:ss" format
	 */
	public GPSPoint(double lat, double lon, String time) {
		super(lat, lon);
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			this.setTimestamp(parser.parse(time));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}

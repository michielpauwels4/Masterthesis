package UserData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.xml.sax.SAXException;

/**
 * 
 * The GPX implementation of the UserDataFacade
 * 
 * @author Michiel Pauwels
 *
 */

public class GPXReader extends UserDataFacade {

	/**
	 * Constructor that sets the name of the directory where the GPX files are
	 * @param dirName
	 */
	public GPXReader(String dirName) {
		super(dirName.split("\\.")[0]);
	}

	@Override
	/**
	 *  Starts a SAXParser that reads all the gpx data in a certain directory
	 */
	public void ReadLines() {
		GPXParser gpxp = new GPXParser();
		File[] files = new File(this.getFileName()).listFiles();
		for(int i=0; i<files.length; i++){
			int prevsize = gpxp.trips.size();
			String fileName = files[i].getPath().split(this.getFileName())[0];
			File file = new File(fileName);
			try {
				gpxp.parse(new FileInputStream(file));
			} catch (SAXException e) {
				System.out.println(e.getMessage());
				System.out.println("catched: No valable gpx file!");
				for(int u=prevsize; u<gpxp.trips.size();u++){
					gpxp.trips.remove(u);
				}
			} catch (FileNotFoundException e) {
			} catch (IOException e) {
			} finally {
				try {
					gpxp.stroom.close();
				} catch (IOException e) {}
			}
			if(gpxp.trips.size()-prevsize <= 0){
				System.out.println("No valable gpx file!");
				System.out.println(file.delete());
			}
			else {
				System.out.println(gpxp.trips.size()-prevsize+" trip(s) ingelezen!");
			}
		}
		trips.addAll(gpxp.trips);
	}

}
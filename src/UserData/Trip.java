package UserData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * A class which holds the user trip information
 * @author Michiel Pauwels
 *
 */

public class Trip {

	private String t_id;
	private Date start;
	private Date end;
	private ArrayList<GPSPoint> route = new ArrayList<GPSPoint>();
	private double SecondsPerPoint;
	
	/**
	 * Simple constructor
	 */
	public Trip() {}

	/**
	 * The constructor method which sets the trip id and parses the date string
	 * @param id The unique id for the trip
	 * @param start The timestamp of the beginning of the trip ("yyyy-MM-dd HH:mm:ss")
	 * @param end The timestamp of the ending of the trip ("yyyy-MM-dd HH:mm:ss")
	 */
	public Trip(String id, String start, String end) {
		this.setT_id(id);
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			this.setStart(parser.parse(start));
			this.setEnd(parser.parse(end));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * Getter method for the list of GPSPoints
	 * @return the set of GPSPoints
	 */
	public ArrayList<GPSPoint> getRoute() {
		return route;
	}

	public String getT_id() {
		return t_id;
	}

	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	public double getSecondsPerPoint() {
		return SecondsPerPoint;
	}

	public void setSecondsPerPoint(double secondsPerPoint) {
		SecondsPerPoint = secondsPerPoint;
	}

}

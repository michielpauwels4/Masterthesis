package UserData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * The CSV implementation of the UserDataFacade
 * 
 * @author Michiel Pauwels
 *
 */
public class CSVReader extends UserDataFacade {

	public CSVReader(String fileName) {
		super(fileName);
	}

	public String toString(String[] lijst) {
		String tekst = "";
		for (int i = 0; i < lijst.length; i++) {
			tekst += lijst[i] + ", ";
		}
		return tekst;
	}

	@Override
	/**
	 * Creates BufferedReader, reads file line per line. 
	 * For each line, the method calls ReadUserData to process the information
	 */
	public void ReadLines() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(this.getFileName()));
			String line = null;
			boolean first = true;
			float accuracy;
			while ((line = br.readLine()) != null) {
				if (first == true) {
					first = false;
				} else {
					String[] gesplitst = line.split(";");
					String[] data = { gesplitst[0], gesplitst[7], gesplitst[8], gesplitst[10], gesplitst[11],
							gesplitst[13], gesplitst[14] };
					try {
						accuracy = Float.parseFloat(gesplitst[6]);
						if (accuracy <= 100) {
							this.ReadUserData(data);
						}
					} catch (NumberFormatException e) {
					}
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

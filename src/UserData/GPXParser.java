package UserData;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * 
 * A class that parses a GPX file.
 * Extends defaulthandler
 * 
 * @author Michiel Pauwels
 *
 */

public class GPXParser extends DefaultHandler {
	
	private SAXParser parser;
	protected InputStream stroom;
	
	protected ArrayList<Trip> trips = new ArrayList<Trip>();
	private Trip newTrip;
	private GPSPoint newPoint;
	private SimpleDateFormat dparser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	
	private boolean time;
	private boolean trkpoint;
	private boolean nieuw;

	/**
	 * Constructor that initializes the parser
	 */
	public GPXParser(){
		
		try {
			parser = SAXParserFactory.newInstance().newSAXParser();
		} catch (ParserConfigurationException e) {
			System.out.println(e);
		} catch (SAXException e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Method that parses an InputStream into a set of trip objects
	 * @param s The InputStream to be parsed
	 * @throws SAXException Thrown if there is something wrong while parsing
	 * @throws IOException Thrown if the InputStream can't be closed
	 */
	public void parse(InputStream s) throws SAXException, IOException {
		stroom = s;
		long startTime = System.nanoTime();
		System.out.println("Start GPX Parsing");
		try {
			parser.parse(stroom,this);
		} catch (IOException e) {
			System.out.println(e);
		}
		long endTime = System.nanoTime();
		System.out.println("Inlezen GPX: " + Math.round(((endTime - startTime) * (Math.pow(10.0, -9.0)))) + "sec");
		s.close();
	}
	
	/**
	 * Method that gets called every time a new XML tag is found.
	 * constructs node and way objects dependent of the XML tag name.
	 * @throws SAXException 
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (nieuw) {
			if (!qName.equals("gpx")) {
				throw new SAXException("root-tag must be \"gpx\", but was instead "+qName);
			}
			time = false;
			nieuw = false;
			trkpoint = false;
			return;
		}
		if (qName.equals("trk")) {
			newTrip = new Trip();
			return;
		}
		if (qName.equals("trkpt")) {
			trkpoint = true;
			newPoint = new GPSPoint(Double.parseDouble(attributes.getValue("lat")),Double.parseDouble(attributes.getValue("lon")));
			return;
		}

		if (qName.equals("time")) {
			if(trkpoint) {
				time = true;
			}
			return;
		}
	}
	
	/**
	 * Method that gets called when parsing strings between tags
	 */
	public void characters(char[] ch,int start,int length) throws SAXException{
		if(time){
			char[] newchar = new char[length];
			for(int i=0; i<length; i++){
				newchar[i] = ch[i+start];
			}
			try {
				newPoint.setTimestamp(dparser.parse(new String(newchar)));
			} catch (ParseException e) {
				System.out.println(new String(newchar));
				throw new SAXException("Timestamp parsing failed!");
			}
		}
	}
	
	/**
	 * Gets called when starting to parse
	 */
	public void startDocument() {
		nieuw = true;
	}
	
	/**
	 * Method that gets called every time a closing XML tag is found.
	 * Set nodes and ways in the network object dependent of the XML tag name.
	 * @throws SAXException 
	 */
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equals("time")) {
			time = false;
		}
		if (qName.equals("trkpt")) {
			if(newPoint.getTimestamp()==null){throw new SAXException("No timestamp was set!");}
			trkpoint = false;
			newTrip.getRoute().add(newPoint);
		}
		if (qName.equals("trk")) {
			if(newTrip.getRoute().size()<2){throw new SAXException("Not enough trackpoints in the trip!");}
			newTrip.setStart(newTrip.getRoute().get(0).getTimestamp());
			newTrip.setEnd((newTrip.getRoute().get(newTrip.getRoute().size()-1).getTimestamp()));
			trips.add(newTrip);
		}
	}
	
}

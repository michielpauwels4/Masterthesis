package UserData;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 
 * A class that clusters GPSPoint data with the QT clusteralgorithm
 * 
 * @author Michiel Pauwels
 *
 */

public class QTClustering {
	
	private double mindiameter;
	
	/**
	 * The method that uses an optimised QT algorithm
	 * @param G The set of datapoints
	 * @param radius The maximal cluster radius
	 * @return A set of clustered datapoints
	 */
	public ArrayList<ArrayList<GPSPoint>> QTClusteringFast(ArrayList<GPSPoint> G, float radius){
		System.out.println("Start QTClustering");
		ArrayList<GPSPoint> C;
		ArrayList<ArrayList<GPSPoint>> clusters = new ArrayList<ArrayList<GPSPoint>>();
		int count,maxcount,index, stopcount=0, originalsize = G.size();
		GPSPoint p,centroid;
		while(G.size()>1){
			System.out.println("G size: "+G.size());
			maxcount = 0; index=0; stopcount=0;
			for(int i=0; i<G.size(); i++){
				p = G.get(i);
				count = 0; 
				if(stopcount>1000){break;}
				for(int u=0; u<G.size();u++){
						if(((G.size()-u)+count)<maxcount){break;}
						else if(p.CalculateDist(G.get(u))<radius){count++;}
				}
				if(count > maxcount) {maxcount = count; index = i; stopcount=0; }
				else {stopcount++;}
			}
			centroid = G.get(index);
			C = new ArrayList<GPSPoint>(Arrays.asList(centroid));
			for(int i=0; i<G.size(); i++){
				if(centroid.CalculateDist(G.get(i))<radius){C.add(G.get(i));}
			}
			System.out.println("Cluster gevonden, size: "+C.size());
			clusters.add(C);
			G.removeAll(C); // G - C
			if(G.size()<originalsize*0.01){return clusters;}
		}
		return clusters;
	}
	
	/**
	 * The method that uses the original QT algorithm.
	 * Used for testing, too slow to be used for real.
	 * @param G The set of datapoints
	 * @param radius The maximal cluster radius
	 * @return A set of clustered datapoints
	 */
	public ArrayList<ArrayList<GPSPoint>> QTClusteringOriginal(ArrayList<GPSPoint> G, float radius){
		System.out.println("Start QTClustering");
		ArrayList<GPSPoint> C;
		ArrayList<ArrayList<GPSPoint>> clusters = new ArrayList<ArrayList<GPSPoint>>();
		ArrayList<ArrayList<GPSPoint>> candidates;
		boolean flag;
		while(G.size()>1){
			System.out.println("G size: "+G.size());
			candidates = new ArrayList<ArrayList<GPSPoint>>();
			for(int i=0; i<G.size(); i++){
				flag = true; candidates.add(new ArrayList<GPSPoint>(Arrays.asList(G.get(i))));
				while(flag==true && candidates.get(i).size()!=G.size()){
					mindiameter = Double.POSITIVE_INFINITY;
					GPSPoint j = findMinimumDiameter(G,candidates.get(i));
					if(mindiameter > radius*2.0){
						flag = false;
					}
					else {
						candidates.get(i).add(j);
					}
				}
			}
			C = getMaxSet(candidates);
			System.out.println("Cluster gevonden, size: "+C.size());
			clusters.add(C);
			G.removeAll(C);
		}
		return clusters;
	}
	
	public GPSPoint findMinimumDiameter(ArrayList<GPSPoint> G, ArrayList<GPSPoint> A){
		int index=-1;
		for(int i=0; i<G.size(); i++){
			if(!A.contains(G.get(i))){
				double diameter=0;
				for(int u=0; u<A.size(); u++){
					if(G.get(i).CalculateDist(A.get(u))>diameter){
						diameter = G.get(i).CalculateDist(A.get(u));
					}
				}
				if(diameter<mindiameter){
					index = i;
					mindiameter = diameter;
				}
			}
		}
		return G.get(index);
	}
	
	/**
	 * Searches the cluster with the most elements
	 * @param candidates The candidate clusters
	 * @return The cluster with the most elements
	 */
	public ArrayList<GPSPoint> getMaxSet(ArrayList<ArrayList<GPSPoint>> candidates){
		int nbelements = 0, index=-1;
		for(int i=0; i<candidates.size(); i++){
			if(candidates.get(i).size()>nbelements){
				index = i;
				nbelements = candidates.get(i).size();
			}
		}
		return candidates.get(index);
	}
}

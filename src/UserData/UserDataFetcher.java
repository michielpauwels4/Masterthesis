package UserData;

import java.util.ArrayList;

/**
 * 
 * A class which gets user datatrips with the right readerclass 
 * and clusters the GPS points
 * 
 * @author Michiel Pauwels
 *
 */

public class UserDataFetcher {
	
	String data;
	QTClustering QT = new QTClustering();
	public ArrayList<Trip> trips;
	private float maxWindow;
	
	/**
	 * The constructor method which sets the filename and maximal radius for clustering
	 * @param fileName The name of the file
	 * @param maxWin The maximal cluster radius
	 */
	public UserDataFetcher(String fileName, float maxWin){
		data = fileName;
		maxWindow = maxWin;
	}
	
	/**
	 * A method which calls the right Readerclass dependent on the file extension.
	 * The trips are read in and clustered
	 * @return A set of clustered datapoints
	 */
	public ArrayList<ArrayList<GPSPoint>> getUserData(){
		UserDataFacade Data;
		String extension = "";
		try {
			extension = data.split("\\.")[1];
		} catch(ArrayIndexOutOfBoundsException e){
			throw new IllegalArgumentException("No extension!");
		}
		if (extension.equals("txt")) {
			Data = new TXTReader(data);
		} else if (extension.equals("csv")) {
			Data = new CSVReader(data);
		} else if (extension.equals("gpx")) {
			Data = new GPXReader(data);
		} else {
			throw new IllegalArgumentException("Unknown extension: "+extension);
		}
		Data.getUserData();
		trips = Data.getTrips();
		return QT.QTClusteringFast(tripsToPoints(trips), maxWindow);
	}
	
	private ArrayList<GPSPoint> tripsToPoints(ArrayList<Trip> trips){
		ArrayList<GPSPoint> returnlist = new ArrayList<GPSPoint>();
		for(int i=0; i<trips.size(); i++){
			for(int u=0; u<trips.get(i).getRoute().size(); u++){
				returnlist.add(trips.get(i).getRoute().get(u));
			}
		}
		return returnlist;
	}
	
}